<?php

namespace Proxies\__CG__\AppBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Cloturation extends \AppBundle\Entity\Cloturation implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'id', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'dateEnvoi', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'dateFinIncident', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'dateDebutIncident', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'dateFinImpact', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'dateDebutImpact', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'clotureur', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'incident'];
        }

        return ['__isInitialized__', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'id', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'dateEnvoi', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'dateFinIncident', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'dateDebutIncident', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'dateFinImpact', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'dateDebutImpact', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'clotureur', '' . "\0" . 'AppBundle\\Entity\\Cloturation' . "\0" . 'incident'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Cloturation $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function setIncident($incident)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIncident', [$incident]);

        return parent::setIncident($incident);
    }

    /**
     * {@inheritDoc}
     */
    public function getIncident()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIncident', []);

        return parent::getIncident();
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateEnvoi($dateEnvoi)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateEnvoi', [$dateEnvoi]);

        return parent::setDateEnvoi($dateEnvoi);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateEnvoi()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateEnvoi', []);

        return parent::getDateEnvoi();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateFinIncident($dateFinIncident)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateFinIncident', [$dateFinIncident]);

        return parent::setDateFinIncident($dateFinIncident);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateFinIncident()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateFinIncident', []);

        return parent::getDateFinIncident();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateDebutIncident($dateDebutIncident)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateDebutIncident', [$dateDebutIncident]);

        return parent::setDateDebutIncident($dateDebutIncident);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateDebutIncident()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateDebutIncident', []);

        return parent::getDateDebutIncident();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateFinImpact($dateFinImpact)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateFinImpact', [$dateFinImpact]);

        return parent::setDateFinImpact($dateFinImpact);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateFinImpact()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateFinImpact', []);

        return parent::getDateFinImpact();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateDebutImpact($dateDebutImpact)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateDebutImpact', [$dateDebutImpact]);

        return parent::setDateDebutImpact($dateDebutImpact);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateDebutImpact()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateDebutImpact', []);

        return parent::getDateDebutImpact();
    }

    /**
     * {@inheritDoc}
     */
    public function setClotureur($clotureur)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setClotureur', [$clotureur]);

        return parent::setClotureur($clotureur);
    }

    /**
     * {@inheritDoc}
     */
    public function getClotureur()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getClotureur', []);

        return parent::getClotureur();
    }

}
