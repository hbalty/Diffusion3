<?php

/* default/settings.html.twig */
class __TwigTemplate_24175965017dfd46d97d56103c0c759522eee64e6e173597e5d709c1c0b15ae4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "default/settings.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        // line 4
        echo "
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>


  
  ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "

";
    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 13
        echo "    
  ";
        // line 14
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 15
            echo "  <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
  ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "  <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
  ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "  <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
  ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "  <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
  ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "  <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
  ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "  <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
  ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "  <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
  ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "  <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
  ";
        }
        unset($context["asset_url"]);
        // line 17
        echo "    
   ";
    }

    // line 23
    public function block_body($context, array $blocks = array())
    {
        // line 24
        echo "
<div class=\"right_col\" role=\"main\">

<div style=\"margin-bottom:4px;\">  
  <small> <a href=\"/\"> Accueil </a> >  <a href=\"/settings\">  <span style=\"color:red\"> Paramètres  </span>  </a>  </small>
</div>

<div class=\"col-md-12\">
  <div class=\"x_panel\">
    <div class=\"x_title\">
    <h2>Paramètres</h2>
    <ul class=\"nav navbar-right panel_toolbox\">
      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
      </li>
    </ul>
    <div class=\"clearfix\"></div>
    </div>
    <div class=\"x_content\">
    
      <!-- Gestion des utilisateurs -->
      
    <a href=\"";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_add");
        echo "\"> <div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_200\">
                          <div class=\"x_title\">
              <br>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i style=\"font-size:50px;color: #009688\" class=\"fa fa-users\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les utilisateurs </h3>
                           

                          </div>
                        </div>
        </div>
    </a>
    
    <!-- Gestion des métiers -->
    <a href=\"";
        // line 66
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("metier_add");
        echo "\">
    <div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_200\">
                          <div class=\"x_title\">
                           <br> 
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-area-chart\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les métiers </h3> <br>

                          </div>
                        </div>
                      </div>
    </a>
    <!--  Gestion des applications -->
     <a href=\"";
        // line 86
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("application_add");
        echo "\">
      <div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_200\">
                          <div class=\"x_title\">
              <br>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-desktop\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les applications </h3>
                            

                          </div>
                        </div>
                      </div>
      </a>
      <!-- Gestion des clients -->
      
          <a href=\"";
        // line 108
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("client_add");
        echo "\">
      <div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_200\">
                          <div class=\"x_title\">
                            <br>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-child\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les clients et templates </h3>

                          </div>
                        </div>
                      </div>
    </a>
      <a href=\"";
        // line 127
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("impact_add");
        echo "\">
      <div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_200\">
                          <div class=\"x_title\">
                            <br>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-crosshairs\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les impacts </h3> <br>

                          </div>
                        </div>
                      </div>
      </a>
      
      <a href=\"";
        // line 147
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categorie_add");
        echo "\">
            <div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_200\">
                          <div class=\"x_title\">
                            Espace dédié aux communications d'informations
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-bookmark-o\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les catégories  </h3>

                          </div>
                        </div>
                      </div>
          </a>
      <a href=\"";
        // line 166
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("vip_add");
        echo "\">
            <div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_200\">
                          <div class=\"x_title\">
                            <br>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-mobile-phone\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les contacts SMS  </h3>

                          </div>
                        </div>
                      </div>
          </a>
      <!-- help -->
            <a href=\"";
        // line 186
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("help_update");
        echo "\">
            <div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_200\">
                          <div class=\"x_title\">
                            <br>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-info\"></i>
                            </div>

                            <h3 class=\"name_title\"> Gérer l'aide  </h3>

                          </div>
                        </div>
                      </div>
          </a>
    </div>
    </div>
    </div>
  </div>
</div>
  





  ";
    }

    // line 220
    public function block_javascripts($context, array $blocks = array())
    {
        // line 221
        echo "      ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "48d23e4_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_bootstrap.min_1.js");
            // line 228
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "48d23e4_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_custom.min_2.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "48d23e4_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_fastclick_3.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "48d23e4_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_ngprogress_4.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "48d23e4_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_switchery.min_5.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "48d23e4_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_fullscreen_6.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
        } else {
            // asset "48d23e4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
        }
        unset($context["asset_url"]);
        // line 230
        echo "      ";
    }

    public function getTemplateName()
    {
        return "default/settings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  392 => 230,  348 => 228,  343 => 221,  340 => 220,  305 => 186,  282 => 166,  260 => 147,  237 => 127,  215 => 108,  190 => 86,  167 => 66,  143 => 45,  120 => 24,  117 => 23,  112 => 17,  62 => 15,  58 => 14,  55 => 13,  52 => 12,  46 => 19,  44 => 12,  34 => 4,  31 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/settings.html.twig", "C:\\xampp\\htdocs\\diffusion\\recette\\Diffusion\\app\\Resources\\views\\default\\settings.html.twig");
    }
}
