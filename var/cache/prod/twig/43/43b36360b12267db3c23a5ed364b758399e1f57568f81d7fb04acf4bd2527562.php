<?php

/* /incident/incidentAdd.html.twig */
class __TwigTemplate_26a4ffcd15823b1a1ab3cb3d4e9f916b3927b40606923093d5f57fa98c16acce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "/incident/incidentAdd.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        // line 4
        echo "<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js\"> </script>
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css\"/>
  ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 18
        echo "
";
    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 12
        echo "      
    ";
        // line 13
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 14
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 16
        echo "      
     ";
    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        // line 22
        echo "
<div class=\"right_col\" role=\"main\" style=\"min-height: 478px;\">

        <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2>Ajouter Incident <small>Décrire l'incident<e l'incident en Français</small></h2>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>
                    
                    ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashBag", array()), "get", array(0 => "Erreur"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 36
            echo "                          <div class=\"alert alert-danger alert-dismissible fade in\" role=\"alert\">
                              ";
            // line 37
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "
                          </div>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "
                        ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashBag", array()), "get", array(0 => "Notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 42
            echo "                          <div class=\"alert alert-warning alert-dismissible fade in\" role=\"alert\">
                              ";
            // line 43
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "
                          </div>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "
                    <h3> Général </h3>
                   
                        ";
        // line 49
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), 'form_start', array("attr" => array("id" => "demo-form2", "data-parsley-validate" => "", "class" => "form-horizontal form-label-left", "novalidate" => "")));
        echo "
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"incident[client]\">Client <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          ";
        // line 55
        if (array_key_exists("client", $context)) {
            // line 56
            echo "                                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "client", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12", "disabled" => "true")));
            echo "
                                ";
        } else {
            // line 58
            echo "                                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "client", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
            echo " 
                           ";
        }
        // line 60
        echo "                        </div>
                      </div>
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Titre <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          ";
        // line 67
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "Titre", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
        echo "
                        </div>
                      </div>
                      
                       <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Type de l'incident <span class=\"required\">*</span> </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          <div id=\"gender\" class=\"btn-group\" data-toggle=\"buttons\">
                                ";
        // line 75
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "Type", array()), 'widget', array("attr" => array("class" => "iCheck-helper")));
        echo " 
                          </div>
                        </div>
                      </div>
                      

                      
                     
                      
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Date Debut de l'incident
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                         <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non ";
        // line 90
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "ddiConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 91
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "DateDebut", array()), 'widget', array("attr" => array("class" => "")));
        echo " </td>
                                
                                </tr>
                        </table>
                        </div>
                      </div>
                      
                      
                       
                       
                       
                      
                        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Durée approximative de résolution 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                           ";
        // line 107
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "dureeResolution", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12", "placeholder" => "Ex : 30 min, 1 heure, ...")));
        echo "   
                        </div>
                      </div>
                        
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Fin Incident 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                           <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  ";
        // line 117
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "dfiConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 118
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "DateFin", array()), 'widget', array("attr" => array("class" => "", "required" => "false")));
        echo "   </td>
                                
                                </tr>
                        </table>
                        </div>
                      </div>
                         

                         
                         

                         
                            <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Description de l'incident <span class=\"required\">*</span> 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 134
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "descTechnique", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => "4")));
        echo "
                        </div>
                      </div>
                            
                             <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Notes internes
                        <div class=\"alert alert-info alert-dismissible fade in\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span>
                        </button>
                       Ces notes ne seront pas prises en compte dans les diffusions.
                         </div>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 147
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "notes", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => "4")));
        echo "
                        </div>
                      </div>


                        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Numéro du REX Jira
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 156
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "rex", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => "4")));
        echo "
                        </div>
                      </div>


                             <hr class=\"half-rule\"/>
                        <h3> Impacts </h3>     
                        <div class=\"form-group\">
                        <label for=\"middle-name\" class=\"control-label col-md-3 col-sm-3 col-xs-12\">Impact <span class=\"required\">*</span> </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          ";
        // line 166
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "impact", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
        echo " 
                        </div>
                      </div>
                             
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Debut Impact
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  ";
        // line 176
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "ddimConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 177
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "DateDebutImpact", array()), 'widget', array("attr" => array("class" => "", "required" => "false")));
        echo "     </td>
                                
                                </tr>
                        </table>
                          
                        </div>
                      </div>
                        
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Fin Impact
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                 <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  ";
        // line 191
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "dfimConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 192
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "DateFinImpact", array()), 'widget', array("attr" => array("class" => "", "required" => "false")));
        echo "   </td>
                                
                                </tr>
                        </table>
                          
                        </div>
                      </div>
                         
                        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Ressenti Utilisateur <span class=\"required\">*</span> 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 204
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "ressentiUtilisateur", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => "4")));
        echo "
                        </div>
                      </div>
                             

                            
                             <div class=\"form-group\">
                                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Les Applications/Metiers impactés <span class=\"required\">*</span> 
                                </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          Application   ";
        // line 214
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "isApplicationImpact", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none", "id" => "tg-button")));
        echo "    Metier 
                                        
                        </div>
                        </div>
                            
                             <div class=\"form-group\" id=\"impactApplication\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Impact Application 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 223
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "impactApplication", array()), 'widget', array("attr" => array("class" => "select2_multiple form-control", "style" => "height:300px;")));
        echo "   
                        </div>
                      </div>
                            
                       
                        <div class=\"form-group\"  id=\"impactMetier\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Impact Metier 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 232
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), "impactMetier", array()), 'widget', array("attr" => array("class" => "select2_multiple form-control", "style" => "height:300px;")));
        echo "   
                        </div>
                      </div>
                      
                      <div class=\"ln_solid\"></div>
                      <div class=\"form-group\" width=\"100px;\">
                        <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                          <input type=\"submit\" class=\"btn btn-success\" value=\"ajouter incident\">
                        </div>
                      </div>
                     
                        ";
        // line 243
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : null), 'form_end');
        echo "

                         <script>
                        var \$client = \$('#incident_client');
                        console.log(\$client) ;
                        // When sport gets selected ...
                        \$client.change(function() {
                          console.log('changed') ;
                          // ... retrieve the corresponding form.
                          var \$form = \$(this).closest('form');
                          // Simulate form data, but only include the selected sport value.
                          var data = {};
                          data[\$client.attr('name')] = \$client.val();
                         

       

                          // Submit data via AJAX to the form's action path.

                          \$.ajax({
                            url : \$form.attr('action'),
                            type: \$form.attr('method'),
                            data : data,
                            success: function(html) {
                          
                               
                              \$('#incident_impactMetier').replaceWith(

                                // ... with the returned one from the AJAX response.
                                \$(html).find('#incident_impactMetier')
                              );

                              \$('#incident_impactApplication').replaceWith(

                                // ... with the returned one from the AJAX response.
                                \$(html).find('#incident_impactApplication')
                              );

                              \$('#incident_impact').replaceWith(

                                // ... with the returned one from the AJAX response.
                                \$(html).find('#incident_impact')
                              );


                           




                              

                            }
                          });
                        });
                        </script>

                    
                  </div>
                </div>
              </div>
            </div>

</div>




";
    }

    // line 313
    public function block_javascripts($context, array $blocks = array())
    {
        // line 314
        echo "            ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "f063a78_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_f063a78_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/f063a78_bootstrap.min_1.js");
            // line 322
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "f063a78_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_f063a78_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/f063a78_custom.min_2.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "f063a78_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_f063a78_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/f063a78_fastclick_3.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "f063a78_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_f063a78_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/f063a78_ngprogress_4.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "f063a78_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_f063a78_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/f063a78_switchery.min_5.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "f063a78_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_f063a78_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/f063a78_fullscreen_6.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "f063a78_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_f063a78_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/f063a78_script_7.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
        } else {
            // asset "f063a78"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_f063a78") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/f063a78.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
        }
        unset($context["asset_url"]);
        // line 324
        echo "      ";
    }

    public function getTemplateName()
    {
        return "/incident/incidentAdd.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  569 => 324,  519 => 322,  514 => 314,  511 => 313,  438 => 243,  424 => 232,  412 => 223,  400 => 214,  387 => 204,  372 => 192,  368 => 191,  351 => 177,  347 => 176,  334 => 166,  321 => 156,  309 => 147,  293 => 134,  274 => 118,  270 => 117,  257 => 107,  238 => 91,  234 => 90,  216 => 75,  205 => 67,  196 => 60,  190 => 58,  184 => 56,  182 => 55,  173 => 49,  168 => 46,  159 => 43,  156 => 42,  152 => 41,  149 => 40,  140 => 37,  137 => 36,  133 => 35,  118 => 22,  115 => 21,  110 => 16,  60 => 14,  56 => 13,  53 => 12,  50 => 11,  45 => 18,  43 => 11,  34 => 4,  31 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "/incident/incidentAdd.html.twig", "C:\\xampp\\htdocs\\diffusion\\recette\\Diffusion\\app\\Resources\\views\\incident\\incidentAdd.html.twig");
    }
}
