<?php

/* default/index.html.twig */
class __TwigTemplate_02f73683252db7fdbb9d9bc494212c91b60b2dd485f25306de25a61cdfdff6fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        // line 4
        echo "
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>


  
  ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "

";
    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 13
        echo "\t  
\t";
        // line 14
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 15
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
        }
        unset($context["asset_url"]);
        // line 17
        echo "\t  
\t ";
    }

    // line 23
    public function block_body($context, array $blocks = array())
    {
        // line 24
        echo "
<div class=\"right_col\" role=\"main\">
<div class=\"col-md-12\">
\t<div class=\"x_panel\">
\t  <div class=\"x_title\">
\t\t<h2 style=\"color:#df3a00\"> Bienvenue</h2>
\t\t<ul class=\"nav navbar-right panel_toolbox\">
\t\t  <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
\t\t  </li>
\t\t</ul>
\t\t<div class=\"clearfix\"></div>
\t  </div>
\t  <div class=\"x_content\">
\t\t\t
\t\t<div class=\"animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12\">
\t\t\t<a href=\"/incident/new\">
\t<div class=\"tile-stats\">
\t  <div class=\"icon\">  <i class=\"fa fa-plus\"></i> </div>
\t  <div class=\"count\">&nbsp</div>
\t  <h3>Ajouter incident</h3>
\t  <p>Créer une nouvelle communication sur incident</p>
\t</div>
\t\t\t\t </a> 
\t\t\t  </div>
\t
\t<a href=\"/incident/list\">
\t\t<div class=\"animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12\">
\t<div class=\"tile-stats\">
\t  <div class=\"icon\"><i class=\"fa fa-bug\"></i></div>
\t  <div class=\"count\"> ";
        // line 53
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["incidents"]) ? $context["incidents"] : null)), "html", null, true);
        echo "</div>
\t  <h3>
\t  ";
        // line 55
        if ((twig_length_filter($this->env, (isset($context["incidents"]) ? $context["incidents"] : null)) > 1)) {
            // line 56
            echo "\t   Incidents en cours
\t   ";
        } else {
            // line 58
            echo "\t   Incident en cours
\t   ";
        }
        // line 60
        echo "\t    </h3>
\t  <p> 
\t  \t";
        // line 62
        if ((twig_length_filter($this->env, (isset($context["incidents"]) ? $context["incidents"] : null)) == 1)) {
            // line 63
            echo "\t  \t";
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["incidents"]) ? $context["incidents"] : null)), "html", null, true);
            echo " incident est en cours
\t  \t";
        } elseif ((twig_length_filter($this->env,         // line 64
(isset($context["incidents"]) ? $context["incidents"] : null)) > 1)) {
            // line 65
            echo "\t  \t";
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["incidents"]) ? $context["incidents"] : null)), "html", null, true);
            echo " incidents sont en cours
\t  \t";
        } else {
            // line 67
            echo "\t  \tAucun incident en cours
\t  \t";
        }
        // line 69
        echo "\t  </p>
\t\t\t</div>
\t\t</div>
\t\t</a>
\t\t  
\t\t  <a href=\"/template/list\">
\t\t<div class=\"animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12\">
\t<div class=\"tile-stats\">
\t  <div class=\"icon\"><i class=\"fa fa-envelope\"></i></div>
\t  <div class=\"count\">  &nbsp </div>
\t  <h3> Mail d'information  </h3>
\t  <p> Afficher les templates prédéfinis </p>
\t\t\t</div>
\t\t</div>
\t\t  </a>
\t\t  
\t  </div>
\t</div>
\t\t\t  </div>
\t
\t<!-- Liste des incidents en cours -->
\t\t\t<div class=\"col-md-12\">
\t<div class=\"x_panel\">
\t  <div class=\"x_title\">
\t\t<h2>Incidents en cours</h2>
\t\t<ul class=\"nav navbar-right panel_toolbox\">
\t\t  <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
\t\t  </li>

\t\t</ul>
\t\t<div class=\"clearfix\"></div>
\t  </div>
\t  <div class=\"x_content\">
\t\t\t
\t\t\t<table class=\"table\">
\t\t  <thead>
\t\t\t<tr>
\t\t\t  <th> Client </th>
\t\t\t  <th>Titre</th>
\t\t\t  <th>Métiers Impactés</th>
\t\t\t  <th> Prochain POS prévu </th>
\t\t\t  <th> Impact </th>
\t\t\t  
\t\t\t  <th> Actions </th>
\t\t\t</tr>
\t\t  </thead>
\t\t  <tbody>
\t\t\t
\t\t\t
\t\t\t ";
        // line 118
        if ((isset($context["incidents"]) ? $context["incidents"] : null)) {
            // line 119
            echo "\t\t\t\t ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["incidents"]) ? $context["incidents"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["incident"]) {
                // line 120
                echo "\t\t\t\t\t<tr>
\t\t\t\t\t <td> ";
                // line 121
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], "client", array()), "clientName", array()), "html", null, true);
                echo " </td>
\t\t\t\t\t <td> ";
                // line 122
                echo twig_escape_filter($this->env, $this->getAttribute($context["incident"], "Titre", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t <td>

\t\t\t\t\t\t ";
                // line 125
                if ( !$this->getAttribute($context["incident"], "isApplicationImpact", array())) {
                    // line 126
                    echo "


\t\t\t\t\t\t ";
                    // line 129
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["incident"], "getMetiersImpactes", array(), "method"));
                    foreach ($context['_seq'] as $context["_key"] => $context["nomMetier"]) {
                        // line 130
                        echo "\t\t\t\t\t\t\t<span class=\"tag\">
\t\t\t\t\t\t\t\t";
                        // line 131
                        echo twig_escape_filter($this->env, $context["nomMetier"], "html", null, true);
                        echo "

\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['nomMetier'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 135
                    echo "
\t\t\t\t
\t\t\t\t\t ";
                } else {
                    // line 138
                    echo "\t\t\t\t\t \t ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["incident"], "impactMetier", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["metier"]) {
                        // line 139
                        echo "\t\t\t\t\t\t\t<span class=\"tag\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["metier"], "nomMetier", array()), "html", null, true);
                        echo "</span>
\t\t\t\t\t\t ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['metier'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 141
                    echo "\t\t\t\t\t ";
                }
                // line 142
                echo "\t\t\t\t\t </td>
\t\t\t\t\t <td>
\t\t\t\t\t\t";
                // line 144
                echo twig_escape_filter($this->env, $this->getAttribute($context["incident"], "getLastPosDate", array(), "method"), "html", null, true);
                echo "
\t\t\t\t\t </td>
\t\t\t\t\t <td>
\t\t\t\t\t\t\t";
                // line 147
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], "impact", array()), "nomImpact", array()), "html", null, true);
                echo "
\t\t\t\t\t </td>
\t\t\t\t\t <td>
\t\t\t\t\t\t<a href=\"";
                // line 150
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pos_add", array("idIncident" => $this->getAttribute($context["incident"], "Id", array()))), "html", null, true);
                echo "\"\" class=\"btn btn-info btn-xs\"><i class=\"fa fa-info\"></i> POS </a>
\t\t\t\t\t\t<a href=\"";
                // line 151
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_update", array("idIncident" => $this->getAttribute($context["incident"], "Id", array()), "idClient" => $this->getAttribute($this->getAttribute($context["incident"], "client", array()), "getId", array(), "method"))), "html", null, true);
                echo "\" class=\"btn btn-info btn-xs\"><i class=\"fa fa-bug\"></i> Modifier </a>
\t\t\t\t\t\t<a href=\"";
                // line 152
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("diffusion_send", array("idIncident" => $this->getAttribute($context["incident"], "Id", array()), "idClient" => $this->getAttribute($this->getAttribute($context["incident"], "client", array()), "getId", array(), "method"))), "html", null, true);
                echo "\" class=\"btn btn-info btn-xs\"><i class=\"fa fa-envelope\"></i> Email </a>
\t\t\t
\t\t\t\t\t\t<a href=\"";
                // line 154
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sms_send", array("idIncident" => $this->getAttribute($context["incident"], "Id", array()))), "html", null, true);
                echo "\" class=\"btn btn-info btn-xs\"> <i class=\"glyphicon glyphicon-phone\"></i> Envoyer SMS  </a>
\t\t\t\t\t </td>
\t\t\t\t\t 
\t\t  
\t
\t
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['incident'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 161
            echo "\t";
        }
        // line 162
        echo "\t\t

\t\t  </tbody>
\t\t</table>
\t\t  
\t\t  
\t  </div>
\t</div>
\t\t\t  </div>
\t
\t
\t<!-- fin liste des incidents en cours -->

\t</div>



  ";
    }

    // line 181
    public function block_javascripts($context, array $blocks = array())
    {
        // line 182
        echo "            ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "48d23e4_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_bootstrap.min_1.js");
            // line 189
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "48d23e4_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_custom.min_2.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "48d23e4_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_fastclick_3.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "48d23e4_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_ngprogress_4.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "48d23e4_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_switchery.min_5.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
            // asset "48d23e4_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4_fullscreen_6.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
        } else {
            // asset "48d23e4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_48d23e4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/48d23e4.js");
            echo "          <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
      ";
        }
        unset($context["asset_url"]);
        // line 191
        echo "      ";
    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  424 => 191,  380 => 189,  375 => 182,  372 => 181,  351 => 162,  348 => 161,  335 => 154,  330 => 152,  326 => 151,  322 => 150,  316 => 147,  310 => 144,  306 => 142,  303 => 141,  294 => 139,  289 => 138,  284 => 135,  274 => 131,  271 => 130,  267 => 129,  262 => 126,  260 => 125,  254 => 122,  250 => 121,  247 => 120,  242 => 119,  240 => 118,  189 => 69,  185 => 67,  179 => 65,  177 => 64,  172 => 63,  170 => 62,  166 => 60,  162 => 58,  158 => 56,  156 => 55,  151 => 53,  120 => 24,  117 => 23,  112 => 17,  62 => 15,  58 => 14,  55 => 13,  52 => 12,  46 => 19,  44 => 12,  34 => 4,  31 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/index.html.twig", "C:\\xampp\\htdocs\\diffusion\\recette\\Diffusion\\app\\Resources\\views\\default\\index.html.twig");
    }
}
