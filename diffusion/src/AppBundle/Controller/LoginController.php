<?php

namespace AppBundle\Controller;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class LoginController extends Controller
{
    /**
     * @Route("/login", name="main_page")
     */

 public function loginAction(Request $request)
    {
        // replace this example code with whatever you need

        $user = new User();
        $user->setLogin("houssem.balti");
        $user->setPassword("Balta470.");
 
       $form = $this->createFormBuilder($user)
       ->add('login',TextType::class)
       ->add('password',PasswordType::class)
       ->add('submit',SubmitType::class,array('label' => 'login'))
       ->getForm();



       $form->handleRequest($request) ;
       if ($form->isSubmitted() && $form->isValid()){
            $user = $form->getData();
            return $this->redirect('log');


       }


       return $this->render('user/login.html.twig',array(
        'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/logout", name="logout")
     */
    public function userLoginAction(Request $request)
    {

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }




}
