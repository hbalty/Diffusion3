<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Incident;
use AppBundle\Entity\Cloturation;
use AppBundle\Form\IncidentType;
use AppBundle\Form\ClosingType;
use AppBundle\Form\ImpactMetierType;
use AppBundle\Form\ImpactApplicationType;
use AppBundle\Form\MetierType;


class IncidentController extends Controller
{
    /**
     * @Route("/incident/list", name="incident_list")
     */

  public function listAction(Request $request)
    {
      $lesIncidents = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('AppBundle:Incident')
      ->createQueryBuilder('c')
			->orderBy('c.dateCreation','DESC')
      ->getQuery()
      ->iterate();

    	return $this->render("incident/incident.html.twig",array(
        'incidents' => $lesIncidents,
        'user' => $this->getUser() ,
      ));
    }




 public function addAction(Request $request,$idClient)
    {

      


      /*
      * Récupération du nom du Client
      */

      $liaisonClient = $this->getDoctrine()
      ->getRepository('AppBundle:Client')
      ->findOneById($idClient);

      $client = $liaisonClient->getClientName();
      $incident = new Incident();
			$incident->setClient($liaisonClient);
      		$incident->setStatut("En cours");
			$incident->setEmetteur($this->getUser());
      $form = $this->createForm(IncidentType::class,$incident);


      /*
      * récupération des données  depuis le  formulaire de description générale de l'incident
      */
      
      $form -> handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()){
        $incident = $form->getData();
				$incident->setClient($liaisonClient);
				
				if (!$incident->getDdiConnue())
				   $incident->setDateDebut(null) ;
					 
				if (!$incident->getDfiConnue())
				   $incident->setDateFin(null) ;
				
				if (!$incident->getDdimConnue())
				   $incident->setDateDebutImpact(null) ;
					 
				if (!$incident->getDfimConnue())
				   $incident->setDateFinImpact(null) ;
        $em = $this -> getDoctrine() -> getEntityManager();
        $em->persist($incident) ;
        $em->flush();
				return $this->redirect("/incident/list");
      }



      return $this->render('/incident/incidentAdd.html.twig',array(
          'client' => $liaisonClient,
          'formDeclarerIncident' => $form->createView(),				
          ));

    }

		
		
		
		/**
     * @Route("/incident/new/", name="nouvel_incident")
     */

 public function ajouterAction(Request $request) //Add without client action
    {
			

      $cdiscount = $this->getDoctrine()->getRepository('AppBundle:Client')->findOneByClientName("Cdiscount") ;
      if ($cdiscount==null)
      {
      	$this->addFlash('Erreur','Veuillez créer le client Cdiscount') ;
      	return $this->redirect('/');
      }
      $incident = new Incident() ;
      $incident->setStatut("En cours");
			$incident->setEmetteur($this->getUser());
			$incident->setClient($cdiscount) ;
			$incident->setDdiConnue(true) ;
			 
      $form = $this->createForm(IncidentType::class,$incident);


      /*
      * récupération des données  depuis le  formulaire de description générale de l'incident
      */
      
      $form -> handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()){

        $incident = $form->getData();
				/*  Vérification des dates connues ou pas  */

				
				if (!$incident->getDdiConnue())
				   $incident->setDateDebut(null) ;
					 
				if (!$incident->getDfiConnue())
				   $incident->setDateFin(null) ;
				
				if (!$incident->getDdimConnue())
				   $incident->setDateDebutImpact(null) ;
					 
				if (!$incident->getDfimConnue())
				   $incident->setDateFinImpact(null) ;
					

        $em = $this -> getDoctrine() -> getEntityManager();
        $em->persist($incident) ;
        $em->flush();
        $this->addFlash('success','La communication est faite avec succès') ;
		return $this->redirect('/incident/list') ;
      }  else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             foreach($errors as $error){
             	$this->addFlash('Erreur',$error->getMessage());
             }
             
                   return $this->render('/incident/incidentAdd.html.twig',array(
                     'formDeclarerIncident' => $form->createView(),
                     'errors' => $errors,
                     'incident' => $incident,
               ));

           }



      return $this->render('/incident/incidentAdd.html.twig',array(
          'formDeclarerIncident' => $form->createView(),
          'incident' => $incident,
          ));
		}
		
		
		
		
		
    /**
     * @Route("/incident/metier/{idMetier}", name="metier_custom_add_metier")
     */

  public function customAddAction(Request $request,$idMetier)
    {

      $metier = $this
      ->getDoctrine()
      ->getRepository('AppBundle:Metier')
      ->findOneById($idMetier);
			
			$incident = new Incident() ;
      $incident->setClient($metier->getClient());
      $incident->setStatut("En cours");
      $incident->addMetierImpacte($metier);
			$form = $this->createForm(IncidentType::class,$incident);
			
			
			$form -> handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()){
        $incident = $form->getData();
        $em = $this -> getDoctrine() -> getEntityManager();
        $em->persist($incident);
        $em->flush();
      }
			
			
			return  $this->render('/incident/incidentMetierAdd.html.twig',array(
          'formDeclarerIncident' => $form->createView(),
					'incident' => $incident,
          ));
	
     

    }
		
		
		
		 /**
     * @Route("/incident/application/{idApplication}", name="incident_custom_add_application")
     */

  public function customappAddAction(Request $request,$idApplication)
    {

      $application = $this
      ->getDoctrine()
      ->getRepository('AppBundle:Application')
      ->findOneById($idApplication);
			
			$incident = new Incident() ;
			$incident->addApplicationImpactee($application);
			
			
			$form = $this->createForm(IncidentType::class,$incident);
			
			
			$form -> handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()){
        $incident = $form->getData();
				$incident->setStatut("En cours");
				$incident->setClient($application->getMetiers()->get(0)->getClient());
		        $em = $this -> getDoctrine() -> getEntityManager();
		        $em->persist($incident) ;
		        $em->flush();
		     }
			
			
			return  $this->render('/incident/incidentApplicationAdd.html.twig',array(
          'formDeclarerIncident' => $form->createView(),
					'incident' => $incident,
          ));
	
     

    }
		
		
		 /**
     * @Route("/incident/clone/{idIncident}", name="incident_clone")
     */

  public function cloneAction(Request $request, $idIncident)
    {
			 $incident = $this->getDoctrine()
			 ->getRepository('AppBundle:Incident')
			 ->findOneById($idIncident) ;
			 
			 $incidentPersist = new Incident();
			 $incidentPersist->setTitre($incident->getTitre());
			 $incidentPersist->setClient($incident->getClient()) ;
			 $incidentPersist->setRessentiUtilisateur($incident->getRessentiUtilisateur()) ;
			 $incidentPersist->setIsApplicationImpact($incident->getIsApplicationImpact());
			 $incidentPersist->setImpact($incident->getImpact() ) ;


			 if (!$incident->getIsApplicationImpact())
			 	$incidentPersist->setImpactApplication($incident->getImpactApplication());
			 else
			 	$incidentPersist->setImpactMetier($incident->getImpactMetier());
			 
			  $incidentPersist->setDateDebut(new \DateTime()) ;
			  $incidentPersist->setDateFin(new \DateTime()) ;
			  $incidentPersist->setDateDebutImpact(new \DateTime()) ;
			  $incidentPersist->setDateFinImpact(new \DateTime()) ;
			  $incidentPersist->setDdiConnue(true);
			  $incidentPersist->setDfiConnue(false);
			  $incidentPersist->setDdimConnue(false);
			  $incidentPersist->setDfimConnue(false);
			 
			 
			 $form = $this->createForm(IncidentType::class,$incidentPersist);
			 $form -> handleRequest($request);
			 if ($form->isSubmitted() && $form->isValid()){
				$incidentPersist = $form->getData();
				if (!$incidentPersist->getDdiConnue())
				   $incidentPersist->setDateDebut(null) ;
					 
				if (!$incidentPersist->getDfiConnue())
				   $incidentPersist->setDateFin(null) ;
				
				if (!$incidentPersist->getDdimConnue())
				   $incidentPersist->setDateDebutImpact(null) ;
					 
				if (!$incidentPersist->getDfimConnue())
				   $incidentPersist->setDateFinImpact(null) ;

				$incidentPersist->setStatut("En cours");
				$incidentPersist->setEmetteur($this->getUser());

				$em = $this->getDoctrine()->getEntityManager() ;
				$em->detach($incident);
				$em->persist($incidentPersist) ;
				$em->flush();
				$this->addFlash('success','L\'incident est ajouté avec succès !') ;
				return $this->redirectToRoute('incident_list') ;
				} else if ($form->isSubmitted() && !$form->isValid()){
             	$data = $form->getData() ;
             	$validator = $this->get('validator');
             	$errors = $validator->validate($data);
             	if (count($errors) > 0 ){
             		foreach($errors as $error){
             			$this->addFlash('Erreur',$error->getMessage());
             		}
             	}
             
                   return $this->render('/incident/incidentClone.html.twig',array(
                     'formDeclarerIncident' => $form->createView(),
                     'errors' => $errors,
               ));

           }
			 
			 	return  $this->render('/incident/incidentClone.html.twig',array(
          'formDeclarerIncident' => $form->createView(),
          'incident' => $incident,
          ));
	
		}
		
		
		/**
     * @Route("/incident/update/{idIncident}", name="incident_update")
     */

  public function updateAction(Request $request, $idIncident)
    {

    		$em = $this->getDoctrine()->getEntityManager() ;


			 $incident = $this->getDoctrine()
			 ->getRepository('AppBundle:Incident')
			 ->findOneById($idIncident) ;
			 
			  $clore = $this->getDoctrine()
			 ->getRepository('AppBundle:Cloturation')
			 ->findOneByIncident($incident) ;

			 // Mettre des dates récentes pour les faire afficher dans le formulaire 
			 if (!$incident->getDdiConnue())
				   $incident->setDateDebut(new \DateTime()) ;
					 
				if (!$incident->getDfiConnue())
				   $incident->setDateFin(new \DateTime()) ;
				
				if (!$incident->getDdimConnue())
				   $incident->setDateDebutImpact(new \DateTime()) ;
					 
				if (!$incident->getDfimConnue())
				   $incident->setDateFinImpact(new \DateTime()) ;


			 $form = $this->createForm(IncidentType::class,$incident);
			 $form -> handleRequest($request);
			 if ($form->isSubmitted() && $form->isValid()){
				$incident = $form->getData();
				if (!$incident->getDdiConnue())
				   $incident->setDateDebut(null) ;
					 
				if (!$incident->getDfiConnue())
				   $incident->setDateFin(null) ;
				
				if (!$incident->getDdimConnue())
				   $incident->setDateDebutImpact(null) ;
					 
				if (!$incident->getDfimConnue())
				   $incident->setDateFinImpact(null) ;

				 if ($incident->getIsApplicationImpact())
			 	$incident->setImpactApplication(new ArrayCollection());
				 else
			 	$incident->setImpactMetier(new ArrayCollection());
				 
				 $validator = $this->get('validator');
            	 $errors = $validator->validate($incident);

            	 if (!$incident->getDfiConnue() && $clore!=null ){
            	 	$incident->setStatut('En cours') ;
            	 	$em->remove($clore) ;
            	 	$em->flush();
            	 } 



				if ($incident->getDfiConnue() && $clore==null){
						$incident->setStatut("Résolu");
						$cloturation = new Cloturation();
						$cloturation->setIncident($incident);
						$cloturation->setDateEnvoi(new \DateTime());
						$cloturation->setDateFinIncident($incident->getDateFin());
						$cloturation->setClotureur($this->getUser());
						$em->persist($cloturation) ; 
						$em->flush();
				}

				$em->flush();
				
				
				return $this->redirectToRoute('incident_list') ;
				}   else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
                   return $this->render('/incident/incidentUpdate.html.twig',array(
                     'formDeclarerIncident' => $form->createView(),
                     'errors' => $errors,
                     'incident' => $incident,
               ));

           }
			 
			 	return  $this->render('/incident/incidentUpdate.html.twig',array(
          'formDeclarerIncident' => $form->createView(),
          'incident' => $incident,
          ));
	
		}
		
		
		
		
		
		
		  /**
     * @Route("/incident/delete/{idIncident}", name="incident_delete")
     */

  public function deleteAction($idIncident)
    {
			$incident= $this->getDoctrine()->getRepository('AppBundle:Incident')
                ->findOneById($idIncident) ;

                if ($incident == null){
                	$this->addFlash('Erreur','L\'incident n\'exsite pas.'); 
                	return $this->redirect('/incident/list');
                }

            $cloturation = $this->getDoctrine()->getRepository('AppBundle:Cloturation')
                ->findOneByIncident($incident) ;

                 $em = $this->getDoctrine()->getManager();
                 if ($cloturation!=null)
                 $em->remove($cloturation) ;
                 $em->remove($incident) ;
                 $em->flush();

                 return $this->redirect("/incident/list");


		}
		
				/**
     * @Route("/incident/history/{idIncident}", name="incident_history")
     */

 public function getHistory(Request $request, $idIncident) //Add without client action
    {
				$incident = $this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident) ;
				$qb = $this->getDoctrine()->getManager()->createQueryBuilder();
				
	$pos = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('AppBundle:Pos')
      ->createQueryBuilder('c')
			->where('c.incident = :incident')
			->setParameter('incident',$incident)
			->orderBy('c.nextPosDate','ASC')
      ->getQuery()->getResult();
			

				
	  $diffusion= $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('AppBundle:Diffusion')
      ->createQueryBuilder('c')
			->where('c.incident = :incident')
			->setParameter('incident',$incident)
			->orderBy('c.dateEnvoi','ASC')
      ->getQuery()->getResult();
			
			$sms =  $this
			->getDoctrine()
      ->getManager()
      ->getRepository('AppBundle:Sms')
      ->createQueryBuilder('c')
			->where('c.incident = :incident')
			->setParameter('incident',$incident)
			->orderBy('c.dateEnvoi','DESC')
      		->getQuery()->getResult();

      	$cloturation =  $this
			->getDoctrine()
		      ->getManager()
		      ->getRepository('AppBundle:Cloturation')
		      ->createQueryBuilder('c')
			->where('c.incident = :incident')
			->setParameter('incident',$incident)
			->orderBy('c.dateEnvoi','DESC')
      		->getQuery()->getResult();


			
		/**
		* Créer un pos virtuelle pour servir comme une cloturation d'un incident
		*/


			$history = array_merge($pos,$diffusion,$sms,$cloturation) ;
			usort ($history,"self::compareDate");
			
			
			return $this->render('/default/history.html.twig',array('history' => array_reverse($history), 'incident' => $incident)) ;

			
			

				
		}
		
		
		 /**
     * @Route("/incident/clore/{idIncident}", name="incident_clore")
     */

			 public function cloreAction(Request $request, $idIncident) // clôturer un incident
				{

			$em = $this->getDoctrine()->getManager();
						
			 $incident = $this->getDoctrine()
			 ->getRepository('AppBundle:Incident')
			 ->findOneById($idIncident) ;
			
			if ($incident->getDdimConnue() && $incident->getDdiConnue() && $incident->getDfiConnue() && $incident->getDfimConnue()){
				$this->addFlash("Notice","L'incident est déjà résolu."); 
				return $this->redirect('/incident/list');
			} 

			$incident->setDateFin(new \DateTime()) ;
			$incident->setDateFinImpact(new \DateTime()) ;
			 
			 $form = $this->createForm(ClosingType::class,$incident);
			 $form -> handleRequest($request);
			  if ($form->isSubmitted() && $form->isValid()){
						$incident = $form->getData();
						$incident->setDfiConnue(1);
						$incident->setDfimConnue(1);
						$incident->setStatut("Résolu");
						$validator = $this->get('validator') ;
						$errors = $validator->validate($incident) ; 
						if (count($errors) > 0 ){
							foreach($errors as $error)
								$this->addFlash('Erreur',$error->getMessage()) ;
							return $this->redirectToRoute('incident_update',array('idIncident' => $incident->getId()));
						}
						// Enregistrement d'une cloturation 
					

						$cloturation = new Cloturation();
						$cloturation->setIncident($incident);
						$cloturation->setDateEnvoi(new \DateTime());
						$cloturation->setDateFinIncident($incident->getDateFin());
						$cloturation->setClotureur($this->getUser());
						$em->persist($cloturation) ; 
						$em->flush();



						
						
						
						return $this->redirectToRoute('incident_list');
					} else if ($form->isSubmitted() && !$form->isValid()){
						$validator = $this->get('validator') ;
						$data = $form->getData(); 
						$errors = $validator->validate($data) ;
						foreach ($errors as $error ){
							$this->addFlash('Erreur',$error->getMessage());
						}
						return $this->redirectToRoute('incident_update',array('idIncident' => $idIncident));
					}
				
			 
			 return $this->render('/incident/incidentClore.html.twig',array('form' => $form->createView(), 'incident' => $incident)) ;
			 
			 
			 }
		
		
		function compareDate($a,$b){
				
				return strcmp($a->getDateEnvoi()->getTimestamp(),$b->getDateEnvoi()->getTimestamp()) ;
			
		}





}
