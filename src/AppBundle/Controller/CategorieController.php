<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\Application;
use AppBundle\Entity\Categorie;
use AppBundle\Form\CategorieType;
use Doctrine\Common\Collections\ArrayCollection;

class CategorieController extends Controller
{
    /**
     * @Route("/categorie/list", name="categorie_list")
     */

 public function listAction(Request $request)
    {

      $categories = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Categorie')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();





          return $this->render('categorie/categorie.html.twig', array('categories' => $categories));

    }


    /**
     * @Route("/categorie/add", name="categorie_add")
     */

 public function addAction(Request $request)
    {
        
         $categories = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Categorie')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();

        

       $categorie = new Categorie();

       // Génération du formulaire de login !
       $form = $this->createForm(CategorieType::class,$categorie);


      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $data->setActivationStatus(true);
             $em = $this->getDoctrine()->getEntityManager() ;
             $em->persist($data);
             $em->flush();
             $this->addFlash('success','La catégorie est ajoutée avec succès.');
             return $this->redirect('/categorie/add');
           }
            else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
             if (count($errors) > 0 ){
                foreach($errors as $error){
                  $this->addFlash('Erreur',$error->getMessage());
                }
             }
                   return $this->render('/categorie/categorieAdd.html.twig',array(
                     'form' => $form->createView(),
                     'categories' => $categories,
                     'pageTitle' => 'Ajouter catégorie',

               ));

           }

      return $this->render('/categorie/categorieAdd.html.twig',array(
       'form' => $form->createView(),
       'categories' => $categories,
       'pageTitle' => 'Ajouter catégorie',
       ));


    }

    /**
     * @Route("/categorie/deactivate/{id}", name="categorie_deactivate")
     */

  public function deactivateAction($id)
    {
         $categorie = $this->getDoctrine()
         ->getRepository("AppBundle:Categorie")
         ->findOneById($id) ;
         if ($categorie->getActivationStatus() == 0 )
              $categorie->setActivationStatus(1) ;
            else {
              $categorie->setActivationStatus(0) ;
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->flush() ;
            if ($categorie->getActivationStatus()){
              $this->addFlash('success','La catégorie est activée avec succès.');
            } else {
              $this->addFlash('success','La catégorie est désactivée avec succès.');
            }

            return $this->redirect('/categorie/add') ;
    }


    /**
     * @Route("/categorie/delete/{id}", name="categorie_delete")
     */

  public function deleteAction($id)
    {
         $categorie = $this->getDoctrine()
         ->getRepository("AppBundle:Categorie")
         ->findOneById($id) ;
         $templates = new ArrayCollection();
         $templates = $this->getDoctrine()
         ->getRepository('AppBundle:Template')
         ->createQueryBuilder('c')
         ->getQuery()
         ->getResult();

         foreach ($templates as $template) {
           if ($template->getCategorie()->contains($categorie)){
            $this->addFlash('Erreur','La catégorie est utilisée dans template. Suppression impossible !');
            return $this->redirectToRoute('categorie_add');
           }
         }

         if ($categorie ==null){
            $this->addFlash('Erreur','La catégorie n\'existe pas!');
            return $this->redirectToRoute('categorie_add');
         }

            $em = $this->getDoctrine()->getManager();
            $em->remove($categorie);
            $em->flush() ;
            
            return $this->redirect('/categorie/add') ;
    }

    

    /**
     * @Route("/categorie/update/{idCategorie}", name="categorie_update")
     */

 public function updateAction(Request $request, $idCategorie)
    {
      // Récupération du la catégorie
      $categorie = $this->getDoctrine()
      ->getRepository('AppBundle:Categorie')
      ->findOneById($idCategorie);


      // Récupération des autres catégories, 
       $categories = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Categorie')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();


      // Création du formulaire
      $form = $this->createForm(CategorieType::class, $categorie) ;

      // handling request

      $form->handleRequest($request) ;

      if($form->isValid() && $form->isSubmitted()){
        $categorie = $form->getData();
        // entity manager

        $em = $this->getDoctrine()->getEntityManager();
        $em->flush();
        $this->addFlash('success','La catégorie est mise à jour avec succès.');
        return $this->redirectToRoute('categorie_add');
      } else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             if (count($errors) > 0){
              foreach($errors as $error){
                $this->addFlash('Erreur',$error->getMessage());
              }
             }
             return $this->redirectToRoute('categorie_add');
           }

      return $this->render('categorie/categorieAdd.html.twig', array(
        'form' => $form->createView(),
        'pageTitle' => 'Modifier catégorie',
        'categories' => $categories,
      )) ;


    }



}
