<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\Help;
use AppBundle\Entity\Categorie;
use AppBundle\Form\CategorieType;

class HelpController extends Controller
{
   

  

    /**
     * @Route("/help/update/", name="help_update")
     */

 public function updateAction(Request $request)
    {
      // Récupération du l'application
      $help = $this->getDoctrine()
      ->getRepository('AppBundle:Help')
      ->findOneById(0);
      // Création du formulaire si l'élément existe déja
      
         $form = $this->createFormBuilder($help)
         ->add('link',TextType::class)
         ->getForm();
         
         $em = $this->getDoctrine()->getManager();

        
      

      // handling request

      $form->handleRequest($request) ;
   
      if($form->isValid() && $form->isSubmitted()){

         
         if ($help == null)
         {
            $help = new Help();
            $data = $form->getData();
            $help->setLink($data['link']);
            $help->setId(0);
            $validator = $this->get('validator');
            $errors = $validator->validate($data);

             if (count($errors) > 0 ){
              foreach($errors as $error){
                $this->addFlash('Erreur',$error->getMessage());
              }
              return $this->redirectToRoute('help_update');
             }

            $em->persist($help) ;
            $em->flush();
            $this->addFlash('success','Le lien est mis à jour avec succès');

            
            return $this->redirectToRoute('help_update');
         } else {
             $data = $form->getData();
             $help->setId(0);
              $validator = $this->get('validator');
              $errors = $validator->validate($data);

             if (count($errors) > 0 ){
              foreach($errors as $error){
                $this->addFlash('Erreur',$error->getMessage());
              }
              return $this->redirectToRoute('help_update');
             }
              $this->addFlash('success','Le lien est mis à jour avec succès');
              $em->flush();
         }
     } else if ($form->isSubmitted() && !$form->isValid()){
         
           $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
              if (count($errors) > 0 ){
              foreach($errors as $error){
                $this->addFlash('Erreur',$error->getMessage());
              }
              return $this->redirectToRoute('help_update');
             }
        
     }
      
        

      return $this->render('help/helpUpdate.html.twig', array(
        'form' => $form->createView(),
      )) ;


    
    }


}
