<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Metier;
use AppBundle\Entity\Client;
use Doctrine\ORM\Query\Expr;
use AppBundle\Form\MetierType;
use AppBundle\Form\MailingListType;

class MetierController extends Controller
{
    /**
     * @Route("/metier/list", name="metier_list")
     */

  public function listAction(Request $request)
    {
        $metiers = $this
        ->getDoctrine()
        ->getManager()
       ->getRepository('AppBundle:Metier')
       ->createQueryBuilder('c')
       ->getQuery()->iterate();
    	 return $this->render("metier/metier.html.twig",array(
       'metiers' => $metiers,
      ));
    }



    /**
     * @Route("/metier/get_by_client/{idClient}", name="metier_get_by_client")
     */

  public function getByClientAction(Request $request, $idClient)
    {
        $client =  $this->getDoctrine()->getRepository('AppBundle:Client')->findOneById($idClient) ;
        $metiers = $this->getDoctrine()->getRepository('AppBundle:Metier')
        ->createQueryBuilder('c')
        ->where('c.client = :client')
        ->setParameter('client',$client)
        ->getQuery()
        ->getResult() ;




        $response = new JsonResponse(array('data' => $metiers)) ; 

       
 
        return $response ; 

    }



    /**
     * @Route("/metier/deactivate/{id}", name="metier_deactivate")
     */

  public function deactivateAction($id)
    {
         $metier = $this->getDoctrine()
         ->getRepository("AppBundle:Metier")
         ->findOneById($id) ;



         if ($metier->getActivationStatus() == 0 )
              $metier->setActivationStatus(1) ;
            else {
              $metier->setActivationStatus(0) ;
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->flush() ;
            if ($metier->getActivationStatus()){
              $this->addFlash('success','Le métier est activé avec succès !');  
            } else {
              $this->addFlash('success','Le métier désactivé avec succès !');
            }
            

            return $this->redirect('/metier/add') ;
    }



		 /**
     * @Route("/metier/details/{idMetier}", name="metier_details")
     */

  public function detailsAction(Request $request, $idMetier)
    {

      $metier = $this
			->getDoctrine()
			->getRepository('AppBundle:Metier')
			->findOneById($idMetier);

    	return $this->render("metier/metierDetails.html.twig",array(
        'metier' => $metier,
        'nomMetier' => $metier->getNomMetier(),
        'id' => $metier->getId(),
      ));
    }


    /**
     * @Route("/metier/add", name="metier_add")
     */

  public function addAction(Request $request)
    {
			

			   $metiers = $this
        ->getDoctrine()
        ->getManager()
       ->getRepository('AppBundle:Metier')
       ->createQueryBuilder('c')
       ->getQuery()->iterate();
					
         $metier = new Metier();
         $metier->setActivationStatus(true);
         $form = $this->createForm(MetierType::class,$metier);
         $form->handleRequest($request);
				 
         if ($form->isSubmitted() && $form->isValid()){
						$metier = $form->getData();
						// Gestion des erreurs existantes dane le formulaire
            $em = $this->getDoctrine()->getManager();
            $em->persist($metier) ;
            $em->flush();
            $this->addFlash('success','Le métier est ajouté avec succès !');
            return $this->redirect('/metier/add') ;

         } else if ($form->isSubmitted() && !$form->isValid()){
					  $metier= $form->getData();
						$validator = $this->get('validator') ;
						$errors = $validator->validate($metier) ;
            foreach($errors as $error){
              $this->addFlash('error',$error->getMessage());
            }
            
            $this->redirectToRoute('metier_add');
						
				 }

         return $this->render("metier/metierAdd.html.twig",array(
           'form' => $form->createView(),
					 'metiers' => $metiers,
         ));

    }


        /**
         * @Route("/metier/update/{idMetier}", name="metier_update")
         */

      public function updateAction(Request $request, $idMetier)
        {
            $metier = $this->getDoctrine()
              ->getRepository('AppBundle:Metier')
              ->findOneById($idMetier);
              if (!$metier){
               
							 return $this->render("metier/metierUpdate.html.twig",array(
              'form' => $form->createView(),
							'errors' => "Ce metier n'existe pas ! "
            ));
              }
							
             $form = $this->createForm(MetierType::class,$metier);
             $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()){
               $metier = $form->getData();
               $em = $this->getDoctrine()->getManager();
               $em->flush();
               $this->addFlash('success','Le métier est mis à jour avec succès.');
                return $this->redirectToRoute('metier_add');

            }  else if ($form->isSubmitted() && !$form->isValid()){
					  $metier= $form->getData();
						$validator = $this->get('validator') ;
						$errors = $validator->validate($metier) ;
						
            if (count($errors) > 0 ){
              foreach($errors as $error){
                $this->addFlash('Erreur',$error->getMessage());
              }
            }
						  return $this->render("metier/metierUpdate.html.twig",array(
              'form' => $form->createView(),
              'nomMetier' => $metier->getNomMetier(),
              'id' => $metier->getId(),
            ));
				 }

            return $this->render("metier/metierUpdate.html.twig",array(
              'form' => $form->createView(),
              'nomMetier' => $metier->getNomMetier(),
              'id' => $metier->getId(),
            ));
        }
}
