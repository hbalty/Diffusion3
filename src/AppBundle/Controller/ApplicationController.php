<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\Application;
use AppBundle\Entity\Metier;
use AppBundle\Form\ApplicationType;
use Psr\Log\LoggerInterface;

class ApplicationController extends Controller
{




    /**
     * @Route("/application/list", name="application_list")
     */

 public function listAction(Request $request)
    {
      $app = new Application();
      $applications = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Application')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();





          return $this->render('application/application.html.twig', array('applications' => $applications));

    }


    /**
     * @Route("/application/add", name="application_add")
     */

 public function addAction(Request $request)
    {
      $applications = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Application')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();

      $application = new Application();

      
       // Génération du formulaire d'ajout d'application !
       $form = $this->createForm(ApplicationType::class,$application);

       $em = $this->getDoctrine()->getEntityManager() ;
      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $application->setNomApplication($data->getNomApplication());
             $application->setActivationStatus($data->getActivationStatus());
             foreach($data->getMetiers() as $metier){
              $metier->getApplications()->add($application);
             }

             
             $validator = $this->get('validator') ; 
             $errors = $validator->validate($application);


             $em->persist($application);
             $em->flush();
             $this->addFlash('success','L\'application est ajoutée avec succès !'); 
             return $this->redirectToRoute('application_add');

           }  else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
             if (count($errors) > 0 ){
              foreach ($errors as $error )
                $this->addFlash('Erreur',$error->getMessage());
             }

           } 

      return $this->render('/application/applicationAdd.html.twig',array(
       'form' => $form->createView(),
       'applications' => $applications,
       'pageTitle' => 'Ajouter une application'
       ));


    }

    /**
     * @Route("/application/deactivate/{id}", name="application_deactivate")
     */

  public function deactivateAction($id)
    {
         $application = $this->getDoctrine()
         ->getRepository("AppBundle:Application")
         ->findOneById($id) ;
         if ($application->getActivationStatus() == 0 )
              $application->setActivationStatus(1) ;
            else {
              $application->setActivationStatus(0) ;
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->flush() ;
            if ($application->getActivationStatus()){
              $this->addFlash('success','L\'application est activée avec succès !');  
            } else {
              $this->addFlash('success','L\'application est désactivée avec succès !');
            }
            

            return $this->redirect('/application/add') ;
    }


    /**
     * @Route("/application/details/{id}", name="application_details")
     */

  public function detailsAction($id)
    {
        $application = $this
        ->getDoctrine()
        ->getRepository('AppBundle:Application')
        ->findOneById($id) ;

        $mailingLists = "";
        foreach ($application->getMetiers() as $metier){
            $mailingLists.=';'.$metier->getMailingLists();
        }




         return $this->render("application/applicationDetails.html.twig",array(
       'application' => $application,
       'nomApplication' => $application->getNomApplication(),
       'id' => $application->getId(),
       'mailingLists' => array_unique(explode(';',$mailingLists)),
      ));


    }


    /**
     * @Route("/application/update/{idApplication}", name="application_update")
     */

 public function updateAction(Request $request, $idApplication)
    {
      // Récupération du l'application
      $application = $this->getDoctrine()
      ->getRepository('AppBundle:Application')
      ->findOneById($idApplication);

           $applications = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Application')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();

      // Création du formulaire
      $form = $this->createForm(ApplicationType::class, $application) ;
      $em = $this->getDoctrine()->getEntityManager();
      // handling request
      $form->handleRequest($request) ;

      if($form->isValid() && $form->isSubmitted()){

        $metiers = $this->getDoctrine()->getRepository('AppBundle:Metier')->createQueryBuilder('c')->getQuery()->getResult();
        foreach ($metiers as $metier) {
            if ($metier->getApplications()->contains($application)){
              $metier->getApplications()->removeElement($application) ;
            } 
        }

        $updatedApplication = $form->getData();
       foreach($updatedApplication->getMetiers() as $metier){
              if (!$metier->getApplications()->contains($metier))
              $metier->getApplications()->add($application);
             }
        // entity manager
        $em = $this->getDoctrine()->getEntityManager();
        $em->flush();
        
        $this->addFlash('success','Le metier est mis à jour avec succès !'); 
        return $this->redirectToRoute('application_add');
        
      } else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
               if (count($errors) > 0 ){
              foreach ($errors as $error )
                $this->addFlash('Erreur',$error->getMessage());
             }    
      }
      return $this->render('application/applicationUpdate.html.twig', array(
        'form' => $form->createView(),
        'applications' => $applications,
        'pageTitle' => 'Mettre à jour l\'application ',
      )) ;


    }



}