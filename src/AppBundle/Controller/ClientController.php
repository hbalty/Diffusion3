<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Form\ClientType;
use AppBundle\Entity\Client;

class ClientController extends Controller
{
    /**
     * @Route("/client/list", name="client_list")
     */

 public function listAction(Request $request)
    {
      $app = new Client();
      $clients = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Client')
            ->createQueryBuilder('c')
            ->getQuery()
            ->iterate();
   
            foreach($clients as $client){
              var_dump($client[0]->getClientName()) ;
              foreach($client[0]->getImpacts() as $impact){
                var_dump($impact->getNomImpact()) ;
              }

            } 
            die ;

          return $this->render('client/client.html.twig', array('clients' => $clients));

    }


    /**
     * @Route("/client/add", name="client_add")
     */

  public function addAction(Request $request){
   // récupération des clients
   $clients = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Client')
            ->createQueryBuilder('c')
            ->getQuery()
            ->iterate();



 
   

   
    // Création d'un objet Client
    $client = new Client();
    $client->setActivationStatus(true) ;



     // Génération du formulaire d'ajout d'un client !
      $form = $this->createForm(ClientType::class,$client); 
      $form->handleRequest($request) ;
    if ($form->isSubmitted() && $form->isValid()){
      $clientToAdd = $form->getData();
      $em = $this->getDoctrine()->getManager() ;
      $em->persist($clientToAdd);
      $em->flush();

      $this->addFlash('success','Le client est ajouté avec succès.');
      
      return $this->redirectToRoute('client_add');
      
    } else if ($form->isSubmitted() && !$form->isValid()){
      $data = $form->getData();
      $validator = $this->get("validator");
      $errors = $validator->validate($data) ;
      
      if (count($errors) > 0 ){
        foreach($errors as $error){
          $this->addFlash('Erreur',$error->getMessage());
        }
      }
     
    }


    return $this->render('/client/clientAdd.html.twig',array(
     'form' => $form->createView(),
     'clients' => $clients,
     ));

  }


    /**
     * @Route("/client/update/{idClient}", name="client_update")
     */

  public function  updateAction(Request $request, $idClient){
     $client = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Client')
            ->findOneById($idClient);

      $form = $this->createForm(ClientType::class,$client); 
      $form->handleRequest($request) ;
    if ($form->isSubmitted() && $form->isValid()){
      $client = $form->getData();
      $em = $this->getDoctrine()->getManager() ;
      $em->flush($client);

      $this->addFlash('success','Le client est mis à jour avec succès.');
      return $this->redirectToRoute('client_add');

    }
    else if ($form->isSubmitted() && !$form->isValid()){
      $data = $form->getData();
      $validator = $this->get("validator");
      $errors = $validator->validate($data);

       if (count($errors) > 0 ){
        foreach($errors as $error){
          $this->addFlash('Erreur',$error->getMessage());
        }
      }
     
    } 


    return $this->render('/client/clientUpdate.html.twig',array(
     'form' => $form->createView(),
     'nomClient' => $client->getClientName(),
     'id' => $client->getId(),
     ));
            
     
             
  }


      /**
     * @Route("/client/deactivate/{id}", name="client_deactivate")
     */

  public function deactivateAction($id)
    {
         $client = $this->getDoctrine()
         ->getRepository("AppBundle:Client")
         ->findOneById($id) ;
         if ($client->getActivationStatus() == 0 )
              $client->setActivationStatus(1) ;
            else {
              $client->setActivationStatus(0) ;
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->flush() ;
            if ($client->getActivationStatus()){
              $this->addFlash('success','Le client est activé avec succès !');  
            } else {
              $this->addFlash('success','Le client est désactivé avec succès !');
            }
            

            return $this->redirect('/client/add') ;
    }




}
