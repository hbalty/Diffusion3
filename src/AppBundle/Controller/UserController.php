<?php
namespace AppBundle\Controller;
use AppBundle\Entity\User;
use AppBundle\Controller\toolBoxController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Ldap\LdapClient as Ldap;
use Symfony\Component\Ldap\Adapter\ExtLdap\Adapter as ldapAdapter;
class UserController extends Controller
{


          /**
           *
           * @Route("/user/test", name="user_test")
           */

        public function testAction()
            {

              $ldap = new Ldap("10.10.129.5");

              $ldap->bind("svc.clinfo@cdbdx.biz","0N52TUF4CnKyL28Vh9tf");
              echo '<pre>';
              $test = $ldap->query("cn=svc.clinfo=dc=cdbdx,dc=biz", "(&(sAMAccountName=houssem.balti))");
              var_dump($test);

              die ;
            }
  /**
   *
   * @Route("/user/list", name="user_list")
   */

public function listAction()
    {

      $users = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:User')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();


            return $this->render('/user/user.html.twig',array(
             'users' => $users,
             ));
    }


    /**
     *
     *@Route("/user/previleges/{user_id}", name="user_previlege")
     */
public function changePrevAction(Request $requests, $user_id){
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneById($user_id);
        if ($user->hasRole("ROLE_ADMIN")){
                $user->removeRole("ROLE_ADMIN") ;
        } else {
                $user->addRole("ROLE_ADMIN") ;
        }
        
        $this->get('fos_user.user_manager')->updateUser($user, false);
        $this->getDoctrine()->getManager()->flush();
        
        return $this->redirect('/user/add');
        
}





  /**
   *
   * @Route("/user/add", name="user_add")
   */

public function addAction(Request $request)
    {
      /*
      * Ajout d'un utilisateur tout en vérifiant son existance dans le LDAP
      */
        
         $users = $this->getDoctrine()
        ->getManager()
        ->getRepository('AppBundle:User')
        ->createQueryBuilder('c')
        ->getQuery()->iterate();
        
        
      // Création d'un tableau pour chercher l'utilisateur dans le LDAP
      $user = new User();
      $addForm = $this->createFormBuilder()
      ->add('userName',TextType::class)
      ->getForm();

      $firstName="";
      $lastName="";
      $userName = "";
      $thumbnail = "";

      $addForm->handleRequest($request) ;
      if ($addForm->isSubmitted() && $addForm->isValid()){
           $data = $addForm->getData();
           $motclef = $data['userName'];


           // Recherche de l'utilisateur dans l'annuaire
          $justthese = array("ou", "sn","cn" , "givenname","displayname","mail","thumbnailPhoto");
           $ds = ldap_connect("10.10.129.5");
           $login = "svc.clinfo"; //Récupération de la variable login
           $pass = "0N52TUF4CnKyL28Vh9tf";
           $r = ldap_bind($ds,"$login@cdbdx.biz","$pass");
           $search = ldap_search($ds, "ou=UTILISATEURS,dc=cdbdx,dc=biz", "cn=*".$motclef."*",$justthese);
           $results = ldap_get_entries($ds, $search);
           $t=0;
           $test="<tr>";
           $count=$results["count"];
           
           $name = "" ;
           $surname = "";

           for ($i=0; $i < $count; $i++) {
               $chaine=$results[$i]["displayname"][0];
               if (isset($results[$i]['thumbnailphoto'][0]))
               $thumbnail = $results[$i]['thumbnailphoto'][0];
               $link=$results[$i]['cn'][0];
               $link=str_replace(" ",".",$link);
               $link=strtr($link,utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'),'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
               $namesurname = explode(" ", $chaine);
               $firstName[$i]=$namesurname[0];
               $lastName[$i]=$namesurname[1];
               $userName[$i] = $link;
              
             }




      } else {
        $firstName="";
        $lastName="";
        $userName = "";
      }




     return $this->render('/user/userAdd.html.twig',array(
      'addForm' => $addForm->createView(),
      'firstname' => $firstName,
      'lastname' => $lastName,
      'username' => $userName,
      'users' => $users,
      ));

}

        /**
         *
         * @Route("/user/persist/{{userName}}", name="user_persist")
         */

        public function persistAction($userName)
          {
              $photo = self::getUserPhoto($userName);
              $user = new User();
              if (isset($photo) && $photo!=null)
              $user->setPhoto($photo);
              $user->setUserName($userName) ;
              $user->setEmail($userName."@cdiscount.com");
              $user->setPassword("NA") ;
              $user->setEnabled(1);
              $em = $this->getDoctrine()->getManager() ;
              try{
                
              $em->persist($user);
              $em->flush();
              
              } catch(\Exception $ex){
              return $this->redirect($this->generateUrl('user_add',array('error' => "l'utilisateur existe déja !")));
              }
              return $this->redirect('/user/add');

          }



          /**
           *
           * @Route("/user/delete/{userID}", name="user_delete")
           */

        public function deleteAction($userID)
            {
                $user= $this->getDoctrine()->getRepository('AppBundle:User')
                ->findOneById($userID) ;
                 $em = $this->getDoctrine()->getManager();
                 try{
                 $em->remove($user) ;
                 $em->flush();                        
                 } catch(\Exception $ex){
                        return $this->redirect($this->generateUrl('user_add',array('error' => "l'utilisateur est déja supprimé ! rafraichissez la page et réessayez")));
                }


                 return $this->redirect("/user/add");


            }


            public function getUserPhoto($username){
               $usernameExploded = explode('.',$username) ;
               $username = $usernameExploded[0]." ".$usernameExploded[1];
               $justthese = array("thumbnailPhoto");
               $ds = ldap_connect("10.10.129.5");
               $login = "svc.clinfo"; //Récupération de la variable login
               $pass = "0N52TUF4CnKyL28Vh9tf";
               $r = ldap_bind($ds,"$login@cdbdx.biz","$pass");
               $search = ldap_search($ds, "ou=UTILISATEURS,dc=cdbdx,dc=biz", "cn=*".$username."*",$justthese);
               $results = ldap_get_entries($ds, $search);
               $t=0;
               $test="<tr>";
               $count=$results["count"];

                   if (isset($results[0]['thumbnailphoto'][0])){
                   $thumbnail = $results[0]['thumbnailphoto'][0];
                   return $thumbnail ;
                 } else {
                   return null ;
                 }






            }





}
