<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\Vip;
use AppBundle\Entity\SmsList;
use AppBundle\Entity\Categorie;
use AppBundle\Form\VipType;
use AppBundle\Form\SmsListType;

class VipController extends Controller
{


    
    
    
    




    /**
     * @Route("/vip/add", name="vip_add")
     */

 public function addAction(Request $request)
    {
        
         $vips = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Vip')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();

        

       $vip = new Vip();

       // Génération du formulaire de login !
       $form = $this->createForm(VipType::class,$vip);


      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $em = $this->getDoctrine()->getEntityManager() ;
             $em->persist($data);
             $em->flush();
             $this->addFlash('success','Le destinataire sms est ajouté avec succès');
             return $this->redirect('/vip/add');
           }
            else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
             if (count($errors) > 0 ){
              foreach($errors as $error){
                $this->addFlash('Erreur',$error->getMessage());
              }
             }

           }

      return $this->render('/vip/vipAdd.html.twig',array(
       'form' => $form->createView(),
       'vips' => $vips,
       'pageTitle' => 'Ajouter destinataire SMS',
       ));


    }



    /**
     * @Route("/vip/delete/{id}", name="vip_delete")
     */

  public function deleteAction($id)
    {
         $vip = $this->getDoctrine()
         ->getRepository("AppBundle:Vip")
         ->findOneById($id) ;
         
         
            $em = $this->getDoctrine()->getManager();
            if ($vip!=null){
              $em->remove($vip);
              $em->flush(); 
              $this->addFlash('success','Le destinataire est supprimé avec succès.');
            } else {
              $this->addFlash('Erreur','Le destinataire n\'existe pas ! rafraichissez la page s\'il vous plait') ;
            }
            
            return $this->redirect('/vip/add') ;
    }

    

    /**
     * @Route("/vip/update/{idVip}", name="vip_update")
     */

 public function updateAction(Request $request, $idVip)
    {
      // Récupération des vips
      $vip = $this->getDoctrine()
      ->getRepository('AppBundle:Vip')
      ->findOneById($idVip);



         $vips = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Vip')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();

      // Création du formulaire
      $form = $this->createForm(VipType::class, $vip) ;

      // handling request

      $form->handleRequest($request) ;

      if($form->isValid() && $form->isSubmitted()){
        $vip = $form->getData();
        // entity manager

        $em = $this->getDoctrine()->getEntityManager();
        $em->flush();
        $this->addFlash('success','Le destinataire sms est mis à jour avec succès') ;
        return $this->redirect('/vip/add');
      } else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);

             if (count($errors) > 0 ){
              foreach($errors as $error){
                $this->addFlash('Erreur',$error->getMessage());
              }
             }




           }

      return $this->render('vip/vipAdd.html.twig', array(
        'form' => $form->createView(),
        'pageTitle' => 'Modifier destinataire SMS',
        'vips' => $vips,
      )) ;


    }
    
        /**
     * @Route("/vip/deactivate/{id}", name="vip_deactivate")
     */

  public function deactivateAction($id)
    {
         $vip = $this->getDoctrine()
         ->getRepository("AppBundle:Vip")
         ->findOneById($id) ;
         if ($vip->getActivationStatus() == 0 )
              $vip->setActivationStatus(1) ;
            else {
              $vip->setActivationStatus(0) ;
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->flush() ;

             if ($vip->getActivationStatus()){
              $this->addFlash('success','Le destinataire est activé avec succès.');
            } else {
              $this->addFlash('success','Le destinataire est désactivé avec succès.');
            }
            
            return $this->redirect('/vip/add') ;
    }



}
