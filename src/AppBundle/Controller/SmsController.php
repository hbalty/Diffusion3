<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Doctrine\Common\Collections\ArrayCollection;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\Diffusion;
use AppBundle\Entity\MailingList;
use AppBundle\Entity\Incident;
use AppBundle\Entity\Client;
use AppBundle\Entity\InformationDiffusion;
use AppBundle\Entity\Template;
use AppBundle\Entity\Sms;
use AppBundle\Form\SmsType;
use AppBundle\Form\MailingListType;
use AppBundle\Form\InformationDiffusionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class SmsController extends Controller
{
     /**
     * @Route("/diffusion/sms/send/{idIncident}", name="sms_send")
     */
   
      public function prepareSMSAction(Request $request,$idIncident){
          // initialisation
          $incident = $this->getDoctrine()->getRepository("AppBundle:Incident")->findOneById($idIncident) ;
          $list = $this->getDoctrine()->getRepository("AppBundle:Vip")->createQueryBuilder('c')
            ->where('c.activationStatus = true')
            ->getQuery()
            ->iterate();
            
       
          $sms = new Sms();
          $sms->setTitle($incident->getClient()->getClientName().' '.$incident->getTitre().' ('.$incident->getStatut().')');
          if ($incident->getDdiConnue()){
             $sms->setText($incident->getDateDebut()->format('d/m/Y H:i').' '.$incident->getRessentiUtilisateur());
           } else {
            $sms->setText($incident->getRessentiUtilisateur());
           }
         
          // retrieving phone numbers
          $numbers = array();
          $names = array() ;
          foreach($list as $personne){
            $numbers[] = $personne[0]->getPhone();
            $names[] = $personne[0]->getName() ;

          }
          $form = $this->createForm(SmsType::class,$sms);
          
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()){
            $sms = $form->getData() ;
            $sms->setEmetteur($this->getUser());
            $messageComplet = $sms->getTitle()."\n".$sms->getText();
            $sms->setIncident($incident);
            $em = $this->getDoctrine()->getManager() ;
            $em->persist($sms);
            $em->flush();
            $this->addFlash('success','Le SMS est envoyé avec succès !'); 
            
            foreach($numbers as $number){
               self::sendSMSAction($number,$messageComplet);
            }
          
            
         
            return $this->redirect("/incident/list") ;           
          } else if ($form->isSubmitted() && !$form->isValid()){
            $data = $form->getData();
            $validator = $this->get('validator') ;
            $errors = $validator->validate($data);
            if (count($errors) > 0){
              foreach($errors as $error){
                $this->addFlash('Erreur',$error->getMessage()) ;
              }
            }
            
              return $this->render('/sms/smsAdd.html.twig',array(
                     'form' => $form->createView(),
                     'vips' => $list,
                     'errors' => $errors,
                     'incident' => $incident,
                     'list' => $names,
               ));
          }
          
                        return $this->render('/sms/smsAdd.html.twig',array(
                     'form' => $form->createView(),
                     'vips' => $list,
                     'incident' => $incident,
                     'list' => $names,
               ));
          
        
     }
     
     
     public  static function sendSMSAction($tel,$message){
     /***********************************************
     * PoC Sending SMS through SFR APIs using JSON *
     ***********************************************/
    /* Authentications  and Message */
    $service_id     = "9100104377";
    $service_pass   = "CGnlETT4";
    $space_id       = "26663";
 
    $recipient  = $tel;
    $message    = $message;
    $media      = "SMS";
     
 
    /* Data arrays */
    $auth           = array(
                        "serviceId"         => $service_id,
                        "servicePassword"   => $service_pass,
                        "spaceId"           => $space_id
                    );  
 
    $message        = array(
                        "to"        => $recipient,
                        "textMsg"       => $message,
                        "media"             => $media
                    ); 
 
    /* Convert into JSON format */
    $auth_json      = urlencode(json_encode($auth));        
    $msg_json       = urlencode(json_encode($message));
 
     
    /* Request Arguments */
    $query_data     ='authenticate='.$auth_json.'&messageUnitaire='.$msg_json;
 
    echo 'query data : '.$query_data.'<br /><br />';
 
 
     
    /* Build the URL Of the Service */
    $send_query     = 'https://www.dmc.sfr-sh.fr/DmcWS/1.5.1/JsonService/MessagesUnitairesWS/addSingleCall?'.$query_data;
 
 
    $arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
); 
  
    /* Getting the result */
    $result         = file_get_contents($send_query,false,stream_context_create($arrContextOptions));
 
 
    echo 'URL : '.$send_query.'<br /><br />';
 
    echo "result : " . $result;
        
     }
 
 
 
 
 







}
