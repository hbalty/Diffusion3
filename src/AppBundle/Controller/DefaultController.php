<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Incident;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\StreamedResponse;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */

 public function indexAction(Request $request)
    {
        								
								
									$qb = $this->getDoctrine()
									->getManager()
									->createQueryBuilder() ;

									// Récupération de tous les incidents non résolus
									
									$qb->select(array('u'))
									->from('AppBundle:Incident', 'u')
									->where('u.statut = :statut')
									->setParameter('statut','En cours')
									->orderBy('u.dateDebut','DESC');
									
									$incidents = $qb->getQuery()->getResult();
										
									// mise à jour des statuts des incidents
									$em = $this->getDoctrine()->getManager();
									$date = new \DateTime();
							
									

									
									
								
								 
									
								
									return $this->render("default/index.html.twig",array(
															'incidents' => $incidents,
															
															)) ;
									
					
					}
					
					
					 /**
     * @Route("/help", name="help")
     */

 public function HelpAction(Request $request)
    {
						
					$help = $this->getDoctrine()->getRepository('AppBundle:Help')->findOneById(0);
					
					return $this->redirect($help->getLink()); 
				}
										
					
					
					
					
					
					
					 /**
     * @Route("/settings/", name="settings")
     */

 public function testAction(Request $request)
    {
		return $this->render('default/settings.html.twig');
	}



 	/**
      * @Route("/export", name="export")
      */

  public function exportAction(Request $request){

  	// connexion 
  	$conn = $this->get('doctrine.dbal.default_connection') ;

  	// création de la requete 

  	$results = $conn->query('select * from incident') ;

  	// déclaration du réponse 
  	$response = new StreamedResponse(); 
  	$test = self::getNomMetierFromIncident(20) ;

  	$response->setCallback(function() use($results){
  		 $handle = fopen('php://output', 'w+');

  				    fputcsv($handle, array_map("utf8_decode",array('Titre','Client',
                             	  'Statut',
                             	  'Description Technique',
                                'Ressenti Utilisateur',
                             	  'Durée approximative de résolution',
                             	  'Date de début', 
                             	  'Date de fin', 
                             	  'Rex',
	                              'Métiers impactés',
	                              'Applications Impactées',
	                              'Impact',
	                             

            				)),';');


  		// Extraction des champs 
  		while( $row = $results->fetch() ) 
       {
            fputcsv($handle,array_map("utf8_decode",array($row['Titre'], self::getNomClientFromIncident($row['id']),
                                  $row['statut'],
                                  $row['descTechnique'],
                                  $row['ressentiUtilisateur'],
                                  $row['dureeResolution'],
                                  $row['DateDebut'],
                                  $row['DateFin'],
                                  $row['rex'],
                                  self::getNomMetierFromIncident($row['id']),
                                  self::getNomApplisFromIncident($row['id']),
                                  self::getNomImpactFromIncident($row['id']),

                   )),';');
       
       }
       //endwhile
       fclose($handle);

  	}) ;

   $response->setStatusCode(200);
   $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
   $response->headers->set('Content-Disposition','attachment; filename="export.csv"');
      
   return $response;     

  }



  public  function getNomMetierFromIncident($idIncident){

  		
  		$incident = $this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident); 
  		$nomsMetiers = "" ;
      if ($incident == null){
        return "" ;
      }
  		foreach($incident->getImpactMetier() as $impact){
  			$nomsMetiers.=$impact->getNomMetier().','; 
  		}
  		 return $nomsMetiers ;

  }


  public function getNomImpactFromIncident($idIncident){
  		$incident = $this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident); 

  		return $incident->getImpact()->getNomImpact();

  }

   public function getNomClientFromIncident($idIncident){
   		$incident = $this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident); 
   		return $incident->getClient()->getClientName() ;
   } 

   public function getNomApplisFromIncident($idIncident){

  		$incident = $this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident); 
  		$nomsAppli = "" ;
  		foreach($incident->getImpactApplication() as $impact){
  			$nomsMetiers.=$impact->getNomApplication().','; 
  		}
  		 return $nomsMetiers ;
   }



 







}