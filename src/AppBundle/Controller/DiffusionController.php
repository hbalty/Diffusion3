<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Doctrine\Common\Collections\ArrayCollection;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\Diffusion;
use AppBundle\Entity\MailingList;
use AppBundle\Entity\Incident;
use AppBundle\Entity\Client;
use AppBundle\Entity\InformationDiffusion;
use AppBundle\Entity\Template;
use AppBundle\Entity\Impact;
use AppBundle\Form\DiffusionType;
use AppBundle\Form\MailingListType;
use AppBundle\Form\InformationDiffusionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
class DiffusionController extends Controller
{

 
    /**
     * @Route("/diffusion/send/{idIncident}/{idClient}", name="diffusion_send")
     */

 public function sendAction(Request $request,$idIncident, $idClient)
    {


            // Initialisation
            $incident = new Incident();
            $client = new Client();
            $impact = new Impact();

            /*  Récupération de l'incident en question  */

            $incident = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Incident')
            ->findOneById($idIncident);

             /*  Récupération de l'impact en question  */
             $impact = $incident->getImpact();


            /*  Récupération du client en question  */

            $client = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Client')
            ->findOneById($idClient) ;
            

            // Récupération des Metiers/Applications Impactés
            $lesListesDeDiffusions  = "";

            if ($incident->getIsApplicationImpact())
            {
                $impactMetiers = $incident->getImpactMetier()->getValues();
                foreach($impactMetiers as $impactMetier){
                 $lesListesDeDiffusions.=';'.$impactMetier->getMailinglists() ;
                }
            } else {

               $impactApplications = $incident->getImpactApplication()->getValues();
               
               foreach($impactApplications as $impactApplication){
                    foreach($impactApplication->getMetiers() as $metier){
                      if ($metier->getActivationStatus())
                     $lesListesDeDiffusions.=';'.$metier->getMailingLists();
                    }
               }

            }


            // transformations des listes de diffusions en tableau

            $tableauDeDiffusion = array_unique(explode(';',$lesListesDeDiffusions));

          
            $validatedEmails = array();
            $i = 0;

            foreach($tableauDeDiffusion as $email){
            if (strlen($email) > 6) {
                $validatedEmails[$i] = $email;
                $i++ ;
              }

            }

    

            $tableauMailsImpacts = explode(';',$incident->getImpact()->getMailingList());
            $impactValidatedEmails = array();

            $i = 0 ;
            foreach($tableauMailsImpacts as $email){
              if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $impactValidatedEmails[$i] = $email;
                $i++ ;
              }
            }


          self::formatTemplate($client->getTemplate(),$incident,$client) ;

            // instantiation d'une diffusion

            $diffusion = new Diffusion();
            $diffusion->setClient($client);
            $diffusion->setIncident($incident);
            $diffusion->setContenu(self::formatTemplate($client->getTemplate(),$incident,$client));
            $diffusion->setDestEnCopie(implode(';',$validatedEmails));
            $diffusion->setDestImpact(implode(';',$impactValidatedEmails));
            $diffusion->setSujet($client->getTemplateSubject().' '.$incident->getTitre().' '.'('.$incident->getStatut().')');
            $diffusion->setEmetteur($this->getUser());
            
        
             $form = $this->createForm(DiffusionType::class,$diffusion);

               $form -> handleRequest($request);
              if ($form->isSubmitted() &&  $form->isValid() ){
              $diffusion = $form->getData();
              $diffusion->setDateEnvoi(new \DateTime());

              //$diffusion->setDestEnCopie($data->getDestEnCopie());
               
              $em = $this->getDoctrine()
              ->getManager();
              $em-> persist($diffusion);
              $em->flush();
              
              // vérifier les emails entrés dans le formulaire
              $chaineListeNonValidated = $diffusion->getDestEnCopie().';'.$diffusion->getDestImpact();
              $chaineListeNonValidated = str_replace(' ', '', $chaineListeNonValidated);

              $toListNonValidated = explode(';',$chaineListeNonValidated);

              // Valider les formats de mails 
              $i = 0 ;
              
              // initialisation des listes des email
              $toListValidated = array();



              foreach($toListNonValidated as $email){
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                  $toListValidated[$i] = $email;
                  $i++ ;
                }
              }

              $message = \Swift_Message::newInstance()
               ->setSubject($diffusion->getSujet())
               ->setFrom('diffusion-noreply@cdiscount.com')
               ->setBcc($toListValidated)
               ->setBody($diffusion->getContenu(),'text/html') ;


               $this->get('mailer')->send($message);
               $successMessage = sprintf('Le mail a été envoyé à %d destinataires',count($toListValidated));
               $this->addFlash('success',$successMessage);
            }


            return  $this->render('diffusion/diffusionAdd.html.twig', array(
               'addForm' => $form->createView(),
               'lesMetiers' => $validatedEmails,
               'totalMail' => array_merge($validatedEmails,$tableauMailsImpacts),
               'incident' => $incident,
                ));
    }



    public  function formatTemplate($template,$incident, $client){

         $template = str_replace('$__titre', $incident->getTitre(),$template) ;
         $template =str_replace('$__client',$client->getClientName(),$template) ;
         if ($incident->getStatut() =="Résolu")
         {
            $template =str_replace('$__statut','<span style="color:#3BB567">'. $incident->getStatut().'</span>',$template);
         } else {
            $template =str_replace('$__statut',$incident->getStatut(),$template);
         }
         
         //$template = str_replace('$__impacts',self::listMetier($incident),$template);
         
         $template = str_replace('<p>&nbsp;</p>','',$template);

         if ($incident->getDureeResolution() != "")
            $template = str_replace('$__duree',"Temps de résolution estimé : ".$incident->getDureeResolution(),$template);
          else 
            $template = str_replace('$__duree',"",$template);


         if ($incident->getDateDebut() != null )
          $template = str_replace('$__dateDebut',$incident->getDateDebut()->format('d/m/Y H:i'),$template);
          else
          $template = str_replace('$__dateDebut',"En cours",$template);

          if ($incident->getDateFin() != null)
            $template = str_replace('$__dateFin',$incident->getDateFin()->format('d/m/Y H:i'),$template);
          else
          $template = str_replace('$__dateFin',"En cours",$template);

         $template = str_replace('$__ressentiUtilisateur',$incident->getRessentiUtilisateur(),$template);
         $template = str_replace('$__desc',$incident->getDescTechnique(),$template);

          if ($incident->getStatut() != "Résolu")
        $template = str_replace('$__intro',"Bonjour,<br><br> Nous rencontrons actuellement un incident. ",$template) ;
        else
        $template = str_replace('$__intro',"Bonjour,<br><br> Nous avons rencontré un incident, à présent résolu.",$template) ;

        $pos = self::formatPos($incident);
        $template=str_replace('$__pos',$pos,$template);


         return $template;



    }



    public function formatPos($incident){
      // récupération des pos et renvoi du html formaté
      $posText = "" ;
      foreach($incident->getPos() as $pos){
        if ($pos->getIsResolution()){
          if ($pos->getNextAction()!=""){
            $dateEnvoi = $pos->getDateEnvoi()->format('d/m/Y à H:i') ;
            $realizedAction = $pos->getRealizedAction();
            $resolutionDate =$pos->getResolutionDate()->format('d/m/Y à H:i');
            $nextAction = $pos->getNextAction();
            $posText=" 
            <p  style='border-bottom:1px black solid;text-align:left;padding-bottom:5px;'>
            <h3>Point de situation du $dateEnvoi</h3>
             <u>Action(s) réalisée(s)</u>  : <br>
            $realizedAction <br> <br>
            <u>Prochaine action</u> :  <br> $nextAction  <br> <br>
            <u>Résolution estimée</u> : $resolutionDate
            </p>
           " .$posText ; 

          } else {
            $dateEnvoi = $pos->getDateEnvoi()->format('d/m/Y à H:i') ;
            $realizedAction = $pos->getRealizedAction();
            $resolutionDate =$pos->getResolutionDate()->format('d/m/Y à H:i');
            $posText=" 
            <p  style='border-bottom:1px black solid;text-align:left;padding-bottom:5px;'>
            <h3>Point de situation du $dateEnvoi</h3>
             <u>Action(s) réalisée(s)</u>  : <br>
            $realizedAction <br> <br>

            <u>Résolution estimée</u> : $resolutionDate
            </p>
           " .$posText ; 

          }

        } else {
           if ($pos->getNextAction()!=""){
          $dateEnvoi = $pos->getDateEnvoi()->format('d/m/Y à H:i') ;
          $realizedAction = $pos->getRealizedAction();
          $datePP = $pos->getNextPosDate()->format('d/m/Y à H:i') ;
          $nextAction = $pos->getNextAction();
           $posText=" 
          <p  style='border-bottom:1px black solid;text-align:left;padding-bottom:5px;'>
          <h3>Point de situation du $dateEnvoi</h3>
          <u>Action(s) réalisée(s)</u> : <br>
          $realizedAction <br> <br>

          <u>Prochaine action</u> :  <br> $nextAction  <br> <br>
          <u>Date du prochain point</u> : $datePP 
          </p>
         ".$posText ; 
       }
       else {

          $dateEnvoi = $pos->getDateEnvoi()->format('d/m/Y à H:i') ;
          $realizedAction = $pos->getRealizedAction();
          $datePP = $pos->getNextPosDate()->format('d/m/Y à H:i') ;
          $nextAction = $pos->getNextAction();
           $posText=" 
          <p  style='border-bottom:1px black solid;text-align:left;padding-bottom:5px;'>
          <h3>Point de situation du $dateEnvoi</h3>
          <u>Action(s) réalisée(s)</u> : <br>
          $realizedAction <br> <br>

          <u>Date du prochain point</u> : $datePP 
          </p>
         ".$posText ; 
       }

        }    
        
      }
       return str_replace('<p>&nbsp;</p>','',$posText);

    }




    public function listMetier($incident){
     $table = "<tr>" ;
     if ($incident->getIsApplicationImpact()){
       foreach($incident->getImpactMetier() as $impactMetier ){
        $table .="<tr class='impacts'> <td style='height: 40px;
                border-bottom: 1px'> ".$impactMetier->getNomMetier()."</td>  <td style='height: 40px;
                border-bottom: 1px'> <img style='width: 30%;' src='http://www.navocap.com/custom/img/navocap/suivi-client-SAEIV.png' /> </td> </tr> " ;
       }
     } else {
        foreach ($incident->getImpactApplication() as $impactApplication){
        $table .="<tr> <td style='height: 40px;
                border-bottom: 1px' >  Application  ".$impactApplication->getMetiers()->get(0)->getNomMetier()."</td>  <td style='height: 40px;
                border-bottom: 1px'>  <img  style='width: 30%;' src='http://www.navocap.com/custom/img/navocap/suivi-client-SAEIV.png' /> </td> </tr> " ;
        }
       }

     return $table ;

    }

    /**
     * @Route("/diffusion/send/{idTemplate}", name="template_diffusion_send")
     */

 public function templateSendAction(Request $request,$idTemplate)
    {
      $variables = new ArrayCollection();
      $client = new Client();
      $template = $this->getDoctrine()->getManager()->getRepository('AppBundle:Template')->findOneById($idTemplate);
      $diffusion = new InformationDiffusion();
      $diffusion->setClient($template->getClient());
      $diffusion->setSujet($template->getSujet());
      $diffusion->setDestinataire($template->getDestinataires());
      $diffusion->setDestEnCopie($template->getDestEnCopie());
      $diffusion->setContenu($template->getTemplate());
      $diffusion->setTemplate($template);
    
      foreach ($template->getVariables() as $key => $value) {
         $variables[$value->getName()] = "" ;
      }
      $diffusion->setVariables($variables);
      $diffusion->setEmetteur($this->getUser());
      $form = $this->createForm(InformationDiffusionType::class,$diffusion);
      $form->handleRequest($request) ;
      
      if ($form->isSubmitted() &&  $form->isValid() ){
      $diffusion = $form->getData();
      // Vérification que toutes les variables ont été remplacé

      foreach ($diffusion->getVariables() as $nomVariable =>  $variable) {

                 if (strpos($diffusion->getContenu(),$nomVariable)!=false)
                {
                 
                  $this->addFlash('Erreur','Veuillez remplacer tous les variables dans le contenu du mail ! ') ;
                  return  $this->render('diffusion/informationDiffusionSend.html.twig', array(
                 'addForm' => $form->createView(),
                 'template' => $template,
                  ));

                } 
      }



      // preparation du contenu
      $destinatairesNonValidated = explode(';',str_replace(' ','',$diffusion->getDestinataire()));


       $toListNonValidated = explode(';',str_replace(' ','',$diffusion->getDestEnCopie()));
        
       // Validation des destinataires en copie ,
       $toListValidated= array();
        $i = 0 ;
              
              foreach($toListNonValidated as $email){
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                  $toListValidated[$i] = $email;
                  $i++ ;
                }
              }

        // validation du mail des destinataires
        
       $i = 0 ;
         $destinatairesValidated= array();     
              foreach($destinatairesNonValidated as $email){
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                  $destinatairesValidated[$i] = $email;
                  $i++ ;
                }
              }


           

       $numberMailErrors = count($toListNonValidated) - count($toListValidated) ; 
       $message = \Swift_Message::newInstance()
               ->setSubject($diffusion->getSujet())
               ->setFrom('diffusion-noreply@cdiscount.com')
               ->setTo($destinatairesValidated)
               ->setCC($toListValidated)
               ->setBody($diffusion->getContenu())
               ->setContentType('text/html; charset=UTF8')
               ->setEncoder(\Swift_Encoding::getBase64Encoding()) ;


               
       $this->get('mailer')->send($message);
       $em = $this->getDoctrine()->getManager();
       $em-> persist($diffusion);
       $em->flush();


        $this->addFlash('success','Le mail à été envoyé à avec succès.');
        return $this->redirectToRoute('template_diffusion_send',array('idTemplate' => $idTemplate));
       
      
    }



       return  $this->render('diffusion/informationDiffusionSend.html.twig', array(
       'addForm' => $form->createView(),
       'template' => $template,
        ));
    }
    
    


    
     /**
     * @Route("/information/diffusion/history", name="information_diffusion_history")
     */

 public function informationHistoryAction(Request $request)
    {
       // $history = $this->getDoctrine()->getRepository('AppBundle:InformationDiffusion')
       //->createQueryBuilder('c')
       //->getQuery()
       //->iterate();
       
        $qb = $this->getDoctrine()
                   ->getManager()
                   ->createQueryBuilder() ;
          
          $qb->select(array('u'))
        ->from('AppBundle:InformationDiffusion', 'u')
        ->orderBy('u.dateEnvoi','DESC');
        
        $history = $qb->getQuery()->getResult();
       
       return  $this->render('diffusion/history.html.twig', array(
       'history' => $history,
        ));
       
           
    }
    
    
         /**
     * @Route("/information/diffusion/history/{idTemplate}", name="information_diffusion_client_history")
     */

 public function informationHistoryTemlpateAction(Request $request, $idTemplate)
    {
     
             $template =  $this
            ->getDoctrine()
            ->getRepository('AppBundle:Template')
            ->findOneById($idTemplate);
      
         $qb = $this->getDoctrine()
                   ->getManager()
                   ->createQueryBuilder() ;
          
          $qb->select(array('u'))
        ->from('AppBundle:InformationDiffusion', 'u')
        ->where('u.template = :template')
        ->setParameter('template',$template) 
        ->orderBy('u.dateEnvoi','DESC');
        
        
        $history = $qb->getQuery()->getResult();
       
       
        return  $this->render('diffusion/history.html.twig', array(
       'history' => $history,
       'template' => $template,
        ));
       
           
    }






}
