<?php

namespace AppBundle\Controller;
use AppBundle\Entity\User;
use AppBundle\Entity\Login;
use AppBundle\Controller\toolBoxController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Ldap\LdapClient;
use Symfony\Component\HttpFoundation\Session\Session;


class LoginController extends Controller
{
    /**
     * Matches /login exactly
     *
     * @Route("/user/login", name="main_page")
     */

 public function loginAction(Request $request)
      {
        /*
        * Fonction de login : récupération de l'utilisateur de la base de données et binding avec l'annuaire.
        */

       // Création d'un objet Login
       $login = new Login();

        // Génération du formulaire de login !
       $form = $this->createFormBuilder($login)
       ->add('userName',TextType::class)
       ->add('Password',PasswordType::class)
       ->add('submit',SubmitType::class,array('label' => 'Se connecter'))
       ->getForm();



       // Récupération des données depuis le formulaire
       $form->handleRequest($request) ;
       if ($form->isSubmitted() && $form->isValid()){
            $loginAttempt = $form->getData();

            $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneByUsername($loginAttempt->getUserName());

            if ($user){
              $toolBoxController = new toolBoxController(); // instantiation de la barre d'outils
              if ($toolBoxController->verifyLdap($loginAttempt->getUserName(),$loginAttempt->getPassword())){

                // Handling user's session
                $session = new Session();
                $session->set('username', $loginAttempt->getUserName());
                $session->getFlashBag()->add('notice', 'You are connected');
                return $this->redirect("/");
              } else {
                   return $this->redirectToRoute('login', array('error' => 'nope !'));
              }

            } else {
               throw $this->createNotFoundException('The user does not exist');
            }

          }


       return $this->render('/user/login.html.twig',array(
        'form' => $form->createView(),
        ));

  }

     /**
     * @Route("/user/logout", name="logout")
     */

 public function logoutAction(Request $request)
    {
        // replace this example code with whatever you need
      $session = $request->getSession();
      $session->invalidate();
      return $this->redirect('login');
    }






}
