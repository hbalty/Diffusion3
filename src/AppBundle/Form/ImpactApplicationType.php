<?php

namespace AppBundle\Form;

use AppBundle\Entity\Incident;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;



class ImpactApplicationType extends AbstractType
{
    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nomImpactApplication',TextType::class)
        ->add('Configuration',CollectionType::class, array(
          'entry_type'   => ChoiceType::class,
          'entry_options'  => array(
       'choices'  => array(
         'Aucun' => 0,
         'Faible' => 1,
         'Moyen' => 2,
         'Fort'=> 3
        ),
       'multiple' => false,
       "expanded" => true ,
    ),
  )) ;
}

    /**
    * @param OptionsResolverInterface $resolver
    */

    public function setDefaultOptions(OptionsResolverInterface $resolver){
      $resolver->setDefaults(array(
        'data_class' => 'AppBundle\Entity\ImpactApplication'
      ));
    }
}
