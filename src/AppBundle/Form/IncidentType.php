<?php

namespace AppBundle\Form;

use AppBundle\Entity\Incident;
use AppBundle\Entity\Metier;
use AppBundle\Entity\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use AppBundle\Form\ImpactMetierType;
use AppBundle\Form\ImpactApplicationType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormInterface;

class IncidentType extends AbstractType
{

    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('client',EntityType::class,array(
          'class' => 'AppBundle:Client',
          'multiple' => false,
          'expanded' => false,
          'required' => true,
          'placeholder' => 'Selectionner un client',
          'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u')->where('u.activationStatus = true');
            },
            'choice_label' => 'clientName',

        ))
        ->add('Titre',TextType::class)
        ->add('DateDebut',DateTimeType::class)
        ->add('dureeResolution',TextType::class)
        ->add('DateFin',DateTimeType::class)
        ->add('DateDebutImpact',DateTimeType::class)
        ->add('DateFinImpact',DateTimeType::class)
        ->add('impact',EntityType::class,array(
          'class' => 'AppBundle:Impact',
          'multiple' => false,
          'expanded' => false,
          'required' => true,
          'attr' => array('id' => "le_impact"),
          'choices' => [],
          'placeholder' => "selectionner niveau d'impact",
            'choice_label' => 'nomImpact',

        ))
        ->add('Type',ChoiceType::class,array(
          'choices' => array(
            'Exploitation' => 'Exploitation',
            'Changement' => 'Changement',
            'Extérieur' => 'Extérieur'
          ),
          'multiple' => false,
          "expanded" => true ,
        ))
        ->add('descTechnique',TextareaType::class)
        ->add('ressentiUtilisateur',TextareaType::class)
        ->add('notes',TextareaType::class)
        ->add('rex',TextType::class)
        ->add('ddiConnue',CheckboxType::class,array(
            'label' => "Date Debut incident Connue"
        ))
        ->add('dfiConnue',CheckboxType::class,array(
          'label' => "Date Fin incident Connue"
        ))
        ->add('ddimConnue',CheckboxType::class,array(
        'label' => "Date Fin incident Connue"
        ))
        ->add('dfimConnue',CheckboxType::class,array(
        'label' => "Date Fin incident Connue"
        ))
        ->add('isApplicationImpact',CheckboxType::class,array(
        'label' => "Impact Application"
        ))
       
        ->add('impactApplication',EntityType::class,array(
          'class' => 'AppBundle:Application',
          'multiple' => true,
          'expanded' => false,
          'required' => false,
          'attr' => array('height','10px'),
          'choices' => [],
            'choice_label' => 'nomApplication',

        ))
        ->add('impactMetier',EntityType::class,array(
          'class' => 'AppBundle:Metier',
          'multiple' => true,
          'expanded' => false,
          'required' => false,
          'attr' => array('height','10px'),
          'choices' => [],
            'choice_label' => 'nomMetier',

        ));




 


          $metierModifier = function (FormInterface $form, $client ){
            $metiers = (null === $client) ? [] : $client->getMetiers()->toArray() ;

   

              $form->add('impactMetier',EntityType::class,array(
              'class' => 'AppBundle:Metier',
              'multiple' => true,
              'expanded' => false,
               'choices' => $metiers,
               'choice_label' => 'nomMetier'


            )) ;



          } ;


            $applicationModifier = function (FormInterface $form, $client ){
            $applications = (null === $client) ? [] : $client->getApplications()->toArray() ;


   

              $form->add('impactApplication',EntityType::class,array(
              'class' => 'AppBundle:Application',
              'multiple' => true,
              'expanded' => false,
               'choices' => $applications,
               'choice_label' => 'nomApplication'


            )) ;



          } ;



            $impactModifier = function (FormInterface $form, $client ){
            $impacts = (null === $client) ? [] : $client->getImpacts()->toArray() ;

   

              $form->add('impact',EntityType::class,array(
              'class' => 'AppBundle:Impact',
              'multiple' => false,
              'expanded' => false,
              'required' => false,
               'choices' => $impacts,
               'choice_label' => 'nomImpact',
               'label' => 'Choisissez un impact',
               'placeholder' => "selectionner niveau d'impact",


            )) ;



          } ;

          



          $builder->get('client')->addEventListener(
            FormEvents::POST_SUBMIT, 
            function (FormEvent $event) use ($metierModifier){
              $client = $event->getForm() -> getData() ;
              
              $metierModifier($event->getForm()->getParent(), $client) ;

              
            }


            ) ;


          $builder->get('client')->addEventListener(
            FormEvents::POST_SUBMIT, 
            function (FormEvent $event) use ($applicationModifier){
              $client = $event->getForm() -> getData() ;
              
              $applicationModifier($event->getForm()->getParent(), $client) ;
              
            }


            ) ;
            
            $builder->get('client')->addEventListener(
            FormEvents::POST_SUBMIT, 
            function (FormEvent $event) use ($impactModifier){
              $client = $event->getForm() -> getData() ;
              
              $impactModifier($event->getForm()->getParent(), $client) ;
              
            }


            ) ;



            $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event){
              $form = $event->getForm() ;

              $data = $event->getData() ;

              $client = $data->getClient() ;

              $metiers = (null === $client) ? [] : $client->getMetiers()->toArray() ;

              $applications = (null === $client) ? [] : $client->getApplications()->toArray() ;

              $impacts = (null === $client) ? [] :  $client->getImpacts()->toArray() ; 





              $form->add('impactMetier',EntityType::class,array(
              'class' => 'AppBundle:Metier',
              'multiple' => true,
              'expanded' => false,
               'choices' => $metiers,
               'choice_label' => 'nomMetier'


            )) ;

              $form->add('impactApplication',EntityType::class,array(
              'class' => 'AppBundle:Application',
              'multiple' => true,
              'expanded' => false,
               'choices' => $applications,
               'choice_label' => 'nomApplication'


            )) ;

               $form->add('impact',EntityType::class,array(
              'class' => 'AppBundle:Impact',
              'multiple' => false,
              'expanded' => false,
              'required' => false,
               'choices' => $impacts,
               'choice_label' => 'nomImpact',
               'label' => 'Choisissez un impact',
               'placeholder' => "selectionner niveau d'impact",


            )) ;

            }

            ) ;



    }





    /**
    * @param OptionsResolverInterface $resolver
    */

    public function setDefaultOptions(OptionsResolverInterface $resolver){
      $resolver->setDefaults(array(
        'data_class' => 'AppBundle\Entity\Incident'
      ));
    }


}
