<?php

namespace AppBundle\Form;

use AppBundle\Entity\Metier;
use AppBundle\Entity\Client;
use AppBundle\Entity\MailingList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\ImpactMetierType;
use AppBundle\Form\ImpactApplicationType;
use Doctrine\ORM\EntityRepository;

class InformationDiffusionType extends AbstractType
{

    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

      /*
       * Disclaimer :
       * According to symfony best practices this form type is supposed
       * to be passed to the controller so it can be handler and rendered
       * however when i tried to use it i encoutred an error that i presume
       * sensioLabs did it to piss me off and since i ran out of cigrattes and
       * i'm not in the mood for debugging i used the same form but directly in the
       * controller. If you like to fix that please be my guest
       * error : something about calling property on a none object ! cool right ?
       */
      
      
        $this->var = $options['constraints'];
        $builder
        ->add('destinataire',TextType::class)
        ->add('destEnCopie',TextareaType::class,array(
          'required' => false ,
          ))
        ->add('sujet',TextType::class, array(
          'required' => true,
        ))
        ->add('variables',CollectionType::class,array(
        'entry_type' => TextType::class,
        'label_format' => '%name%' ,
        ))
        ->add('contenu',CKEditorType::class, array(
        'config' => array(
        'uiColor' => '#ffffff',
        'height' => '500',
        'allowedContent' => true,
        ), 
        )) ->add('sujet',TextType::class);
          
}

    /**
    * @param OptionsResolverInterface $resolver
    */

    public function setDefaultOptions(OptionsResolverInterface $resolver){
      var_dump($options['constraints']);

      $resolver->setDefaults(array(
        'data_class' => 'AppBundle\Entity\InformationDiffusion',

      ));
    }


}
