<?php

namespace AppBundle\Form;

use AppBundle\Entity\Application;
use AppBundle\Entity\Client;
use AppBundle\Entity\Metier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\ImpactMetierType;
use AppBundle\Form\ImpactApplicationType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Doctrine\ORM\EntityRepository;
use AppBundle\Form\EventListener\AddMetierFieldSubscriber;
use Doctrine\Common\Collections\Criteria;

class ApplicationType extends AbstractType
{

    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        

        $builder
            ->add('client',EntityType::class,array(
          'class' => 'AppBundle:Client',
          'query_builder' => function(EntityRepository $er){
            return $er->createQueryBuilder('u')
            ->where('u.activationStatus = true');
          },
          'required' => true ,
          'choice_label' => 'clientName',
          'placeholder' => 'selectionner un client',
        ))
          ->add('nomApplication',TextType::class)
          ->add('metiers',EntityType::class,array(
              'class' => 'AppBundle:Metier',
              'multiple' => true,
              'expanded' => false,
               'choice_label' => 'nomMetier',
               'choices' => [],


            ))

          ->add('activationStatus',CheckboxType::class,array(
            'label' => 'Active',
            'required' => false));


          

          $formModifier = function (FormInterface $form, $client ){

            $metiers = (null === $client) ? [] : $client->getMetiers()->toArray() ;

   

              $form->add('metiers',EntityType::class,array(
              'class' => 'AppBundle:Metier',
              'multiple' => true,
              'expanded' => false,
               'choices' => $metiers,
               'choice_label' => 'nomMetier'


            )) ;



          } ;
          



          $builder->get('client')->addEventListener(
            FormEvents::POST_SUBMIT, 
            function (FormEvent $event) use ($formModifier){
              $client = $event->getForm() -> getData() ;
              
              $formModifier($event->getForm()->getParent(), $client) ;
            }


            ) ;


            $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event){
              $form = $event->getForm() ;

              $data = $event->getData() ;

              $client = $data->getClient() ;

              $metiers = (null === $client) ? [] : $client->getMetiers()->toArray() ;


              $form->add('metiers',EntityType::class,array(
              'class' => 'AppBundle:Metier',
              'multiple' => true,
              'expanded' => false,
               'choices' => $metiers,
               'choice_label' => 'nomMetier'


            )) ;

            }

            ) ;
            

}

    /**
    * @param OptionsResolverInterface $resolver
    */

    public function setDefaultOptions(OptionsResolverInterface $resolver){
      $resolver->setDefaults(array(
        'data_class' => 'AppBundle\Entity\Application'
      ));
    }


}
