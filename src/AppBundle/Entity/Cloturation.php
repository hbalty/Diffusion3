<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cloturation
 *
 * @ORM\Table(name="cloturation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CloturationRepository")
 */
class Cloturation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnvoi", type="datetime", nullable=true)
     */
    private $dateEnvoi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFinIncident", type="datetime")
     */
    private $dateFinIncident;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebutIncident", type="datetime", nullable=true)
     */
    private $dateDebutIncident;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFinImpact", type="datetime", nullable=true)
     */
    private $dateFinImpact;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebutImpact", type="datetime", nullable=true)
     */
    private $dateDebutImpact;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="clotureur", type="object")
     */
    private $clotureur;





    /**
     * One closing has One Sincident.
     * @ORM\OneToOne(targetEntity="Incident",orphanRemoval=false)
     * @ORM\JoinColumn(name="incident_id", referencedColumnName="id")
     */
    private $incident ;

    /**
     * Set idIncident
     *
     *
     * @return Clouration
     */
    public function setIncident($incident)
    {
        $this->incident = $incident;

        return $this;
    }

    /**
     * Get idIncident
     *
     * @return Incident
     */
    public function getIncident()
    {
        return $this->incident;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateEnvoi
     *
     * @param \DateTime $dateEnvoi
     *
     * @return Cloturation
     */
    public function setDateEnvoi($dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;
    
        return $this;
    }

    /**
     * Get dateEnvoi
     *
     * @return \DateTime
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * Set dateFinIncident
     *
     * @param \DateTime $dateFinIncident
     *
     * @return Cloturation
     */
    public function setDateFinIncident($dateFinIncident)
    {
        $this->dateFinIncident = $dateFinIncident;
    
        return $this;
    }

    /**
     * Get dateFinIncident
     *
     * @return \DateTime
     */
    public function getDateFinIncident()
    {
        return $this->dateFinIncident;
    }

    /**
     * Set dateDebutIncident
     *
     * @param \DateTime $dateDebutIncident
     *
     * @return Cloturation
     */
    public function setDateDebutIncident($dateDebutIncident)
    {
        $this->dateDebutIncident = $dateDebutIncident;
    
        return $this;
    }

    /**
     * Get dateDebutIncident
     *
     * @return \DateTime
     */
    public function getDateDebutIncident()
    {
        return $this->dateDebutIncident;
    }

    /**
     * Set dateFinImpact
     *
     * @param \DateTime $dateFinImpact
     *
     * @return Cloturation
     */
    public function setDateFinImpact($dateFinImpact)
    {
        $this->dateFinImpact = $dateFinImpact;
    
        return $this;
    }

    /**
     * Get dateFinImpact
     *
     * @return \DateTime
     */
    public function getDateFinImpact()
    {
        return $this->dateFinImpact;
    }

    /**
     * Set dateDebutImpact
     *
     * @param \DateTime $dateDebutImpact
     *
     * @return Cloturation
     */
    public function setDateDebutImpact($dateDebutImpact)
    {
        $this->dateDebutImpact = $dateDebutImpact;
    
        return $this;
    }

    /**
     * Get dateDebutImpact
     *
     * @return \DateTime
     */
    public function getDateDebutImpact()
    {
        return $this->dateDebutImpact;
    }

    /**
     * Set clotureur
     *
     * @param \stdClass $clotureur
     *
     * @return Cloturation
     */
    public function setClotureur($clotureur)
    {
        $this->clotureur = $clotureur;
    
        return $this;
    }

    /**
     * Get clotureur
     *
     * @return \stdClass
     */
    public function getClotureur()
    {
        return $this->clotureur;
    }
}

