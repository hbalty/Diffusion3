<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Impact
 *
 * @ORM\Table(name="impact")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImpactRepository")
 */
class Impact
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

  
    /**
    *@Assert\NotBlank(message="Veuillez choisir un client ! ")
    * @ORM\ManyToOne(targetEntity="Client",inversedBy="impacts")
    * @ORM\JoinColumn(nullable=false)
    */
    private $client;




    /**
     * @var string
     *@Assert\NotBlank(message="Le nom d'impact ne doit pas être vide ! ")
     * @ORM\Column(name="nomImpact", type="string", length=255)
     */
    private $nomImpact;

    /**
     * @var bool
     *
     * @ORM\Column(name="activationStatus", type="boolean")
     */
    private $activationStatus;
    
    
    
     /**
     * @var bool
     *
     * @ORM\Column(name="importance", type="boolean",nullable=true)
     */
    private $importance ;
    


    /**
     * @var string
     *@Assert\NotBlank(message="Veuillez Entrer les mailing listes ! ")
     * @ORM\Column(name="mailingList", type="string", nullable=false)
     */
    private $mailingList;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param \stdClass $client
     *
     * @return Impact
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \stdClass
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set nomImpact
     *
     * @param string $nomImpact
     *
     * @return Impact
     */
    public function setNomImpact($nomImpact)
    {
        $this->nomImpact = $nomImpact;

        return $this;
    }

    /**
     * Get nomImpact
     *
     * @return string
     */
    public function getNomImpact()
    {
        return $this->nomImpact;
    }
    
    
    /**
     * Set importance
     *
     * @param boolean $importance
     *
     * @return Impact
     */
    public function setImportance($importance)
    {
        $this->importance = $importance;

        return $this;
    }

    /**
     * Get importance
     *
     * @return bool
     */
    public function getImportance()
    {
        return $this->importance;
    }

    /**
     * Set activationStatus
     *
     * @param boolean $activationStatus
     *
     * @return Impact
     */
    public function setActivationStatus($activationStatus)
    {
        $this->activationStatus = $activationStatus;

        return $this;
    }

    /**
     * Get activationStatus
     *
     * @return bool
     */
    public function getActivationStatus()
    {
        return $this->activationStatus;
    }

    /**
     * Set mailingList
     *
     * @param \stdClass $mailingList
     *
     * @return Impact
     */
    public function setMailingList($mailingList)
    {
        $this->mailingList = str_replace(' ','',$mailingList);

        return $this;
    }

    /**
     * Get mailingList
     *
     * @return \stdClass
     */
    public function getMailingList()
    {
        return $this->mailingList;
    }
}

