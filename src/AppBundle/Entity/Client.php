<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Metier;
use AppBundle\Entity\Application;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientRepository")
 */
class Client
{


    public function __construct() {
        $this->metiers = new ArrayCollection();
        $this->applications = new ArrayCollection();
        $this->impacts = new ArrayCollection() ;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var boolean
     *
     * @ORM\Column(name="activationStatus", type="boolean")
     */
    private $activationStatus;



    /**
     * Set activationStatus
     *
     * @param boolean $activationStatus
     *
     * @return Client
     */

    public function setActivationStatus($activationStatus)
    {
        $this->activationStatus = $activationStatus;

        return $this;
    }

    /**
     * Get activationStatus
     *
     * @return boolean
     */
    public function getActivationStatus()
    {
        return $this->activationStatus;
    }



    /**
     * @var string
     *@Assert\NotBlank(message = "Le nom client ne doit pas être vide !")
     *
     * @ORM\Column(name="ClientName", type="string", length=255, unique=true)
     */
    private $clientName;


    /**
     * One Client has Many metiers.
     * @ORM\OneToMany(targetEntity="Metier", mappedBy="client")
     * @ORM\OrderBy({"nomMetier" = "ASC"})
     */

    private $metiers;


     /**
     * One Client has Many applications.
     * @ORM\OneToMany(targetEntity="Application", mappedBy="client")
       @ORM\OrderBy({"nomApplication" = "ASC"})
     */

    private $applications;



    /**
     * One Client has Many impacts.
     * @ORM\OneToMany(targetEntity="Impact", mappedBy="client")
     * @ORM\OrderBy({"nomImpact" = "ASC"})
     */

    private $impacts;



    /**
     * Get getImpacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */

       public function getImpacts(){
        
        $criteria = Criteria::create()->where(Criteria::expr()->eq("activationStatus", true));
        return $this->impacts->matching($criteria) ;
    }



    /**
     * Get getMetiers
     *
     * @return \Doctrine\Common\Collections\Collection
     */

       public function getMetiers(){
         $criteria = Criteria::create()->where(Criteria::expr()->eq("activationStatus", true));
        return  $this->metiers->matching($criteria) ;

    }


    /**
     * Get getApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */

       public function getApplications(){
       $criteria = Criteria::create()->where(Criteria::expr()->eq("activationStatus", true));
      return $this->applications->matching($criteria);
    }



    /**
     * @var string
     *
     *@Assert\NotBlank(message = "La langue du client ne doit pas être vide !")
     * @ORM\Column(name="Lang", type="string", length=255)
     */
    private $lang;

    /**
     * @var string
     *@Assert\NotBlank(message = "Veuillez renseigner le template du client !")
     * @ORM\Column(name="Template", type="text")
     */
    private $template;






    /**
     * @var string
     *@Assert\NotBlank(message = "Veuillez renseigner le timezone ! ")
     * @ORM\Column(name="Timezone", type="string", length=3000)
     */
    private $timezone;



    /**
     * @var string
     *
     * @ORM\Column(name="templateSubject", type="string", length=3000)
     */
    private $templateSubject;






    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clientName
     *
     * @param string $clientName
     *
     * @return Client
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;

        return $this;
    }

    /**
     * Get clientName
     *
     * @return string
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * Set lang
     *
     * @param string $lang
     *
     * @return Client
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set template
     *
     * @param string $template
     *
     * @return Client
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }




    /**
     * Get templateSubject
     *
     * @return string
     */
    public function getTemplateSubject()
    {
        return $this->templateSubject;
    }


    /**
     * Set templateInfo
     *
     * @param string $templateInfo
     *
     * @return Client
     */
    public function setTemplateSubject($templateSubject)
    {
        $this->templateSubject = $templateSubject;

        return $this;
    }

    /**
     * Set templateInfo
     *
     * @param string $templateInfo
     *
     * @return Client
     */
    public function setTemplateInfo($templateInfo)
    {
        $this->templateInfo = $templateInfo;

        return $this;
    }

    /**
     * Get templateInfo
     *
     * @return string
     */
    public function getTemplateInfo()
    {
        return $this->templateInfo;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     *
     * @return Client
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;


        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }
}
