<?php

namespace AppBundle\Entity;
use AppBundle\Entity\Metier;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Client;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Application
 *  @UniqueEntity(
 *  fields={"client", "nomApplication"},
 * errorPath="nomApplication",
 *  message="Le client a déja une application portant ce nom." 
 * )
 * @ORM\Table(name="application")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicationRepository")
 */
class Application
{
    /**
     * @var int
    *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     *@Assert\NotBlank(message=" Veuillez choisir un client !")
     *

     * @ORM\ManyToOne(targetEntity="Client", inversedBy="applications")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */

    private $client;



    public function getMailingLists(){
        $mailingLists = "" ;
        foreach ($this->metiers as $metier) {
            $mailingLists.=";".$metier->getMailingLists();
        }

        $mailingListArray = array_unique(explode(';', $mailingLists)) ;

        return $mailingListArray ;
    }



    public function getActivatedMetiers(){
        $criteria = Criteria::create()->where(Criteria::expr()->eq("activationStatus", true));
        return $this->metiers->matching($criteria);
    }


    /**
     * @Assert\IsTrue(message = "Veuillez renseigner au moins un métier.")
     */
    public function isMetierDefined() // verifies que la date de debut d'incident est inférieure à la date de fin d'incident
    {
        return  (count($this->getMetiers()) > 0 ) ;
    }

     /**
         *
         * Get client
         *
         * @return object
         */
        public function getClient()
        {
            return $this->client;
        }


        /**
         * Set client
         *
         * @param object $nomMetier
         *
         * @return Metier
         */
        public function setClient($client)
        {
            $this->client = $client;

            return $this;
        }

    

    /**
     * @var string
     * @Assert\NotBlank(message = "Le champ nom application ne doit pas être vide !")
     * @ORM\Column(name="nomApplication", type="string", length=255)
     */
    private $nomApplication;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activationStatus", type="boolean")
     */
    private $activationStatus;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }





    /**
     * Set nomApplication
     *
     * @param string $nomApplication
     *
     * @return Application
     */
    public function setNomApplication($nomApplication)
    {
        $this->nomApplication = $nomApplication;

        return $this;
    }

    /**
     * Get nomApplication
     *
     * @return string
     */
    public function getNomApplication()
    {
        return $this->nomApplication;
    }

    /**
     * Set activationStatus
     *
     * @param boolean $activationStatus
     *
     * @return Application
     */

    public function setActivationStatus($activationStatus)
    {
        $this->activationStatus = $activationStatus;

        return $this;
    }

    /**
     * Get activationStatus
     *
     * @return boolean
     */
    public function getActivationStatus()
    {
        return $this->activationStatus;
    }


    /**
     *@Assert\NotBlank(message="Veuillez choisir au moins un metier !")
     * Plusieurs Metiers peuvent avoir à plusieurs applications
     * @ORM\ManyToMany(targetEntity="Metier", mappedBy="applications",cascade={"persist","remove"})
     */

    private $metiers ;

    public function __construct(){
        $this->metiers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addMetier(Metier $metier)
   {
        $metier->addApplication($this);
       $this->metiers[] = $metier;
   }


   public function getMetiers(){
      return $this->metiers;
   }


       public function setMetiers(Metier $metier){
          $this->metiers = $metier;
          return $this;
       }


}
