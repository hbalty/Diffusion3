<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Pos
 *
 * @ORM\Table(name="pos")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PosRepository")
 */

class Pos
{


    public function __construct(){
        $this->resolutionDate = new \DateTime() ;
        $this->datePos = new \DateTime() ;
        $this->nextPosDate = new \DateTime() ;
        $this->dateEnvoi = new \DateTime() ;
    }
    
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnvoi", type="datetime")
     */
    private $dateEnvoi; // Date de création du pos

    
    public function setDateEnvoi($dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;

        return $this;
    }

    /**
     * Get dateEnvoi
     *@Assert\NotBlank(message = "La date d'envoie doit exister contactez un admin !")
     * @return \DateTime
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ResolutionDate", type="datetime",nullable=true)
     */
    private $resolutionDate;



       /**
     * @var \DateTime
     *
     * @ORM\Column(name="datePos", type="datetime")
     */

    private $datePos;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="nextPosDate", type="datetime",nullable=true)
     */

    private $nextPosDate;


    /**
     * @var string
     *@Assert\NotBlank(message = "Le champ action réalisée doit être renseigné !")
     * @ORM\Column(name="RealizedAction", type="string", length=800)
     */
    private $realizedAction;

    /**
     * @var string
     *
     * @ORM\Column(name="NextAction", type="string", length=800, nullable=true)
     */
    private $nextAction;

    /**
    * Many POS have One Incident.
    * @ORM\ManyToOne(targetEntity="Incident", inversedBy="pos")
    * @ORM\JoinColumn(name="id_incident", referencedColumnName="id")
    */
    private $incident;

    /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(nullable=true)
    */

    private $userDeclare;

    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isResolution", type="boolean")
     */
    private $isResolution ;  // Date de résolution connue (true ou false)
    
    
    
   
    
    
    /**
     * Get isResolution
     *
     * @return boolean
     */
    public function getIsResolution()
    {
        return $this->isResolution;
    }
    

    
    /**
     * Set drConnue
     *
     * @param \DateTime $nextPosDate
     *
     * @return Pos
     */
    public function setNextPosDate($nextPosDate)
    {
        $this->nextPosDate = $nextPosDate;

        return $this;
    }
    
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    
    
    
    
    /**
     * Set isResolution
     *
     * @param boolean $isResolution
     *
     * @return Pos
     */
    public function setIsResolution($isResolution)
    {
        $this->isResolution = $isResolution;

        return $this;
    }
    
    
     /**
     * Get nextPosDate
     *
     * @return \DateTime
     */
    public function getNextPosDate()
    {
        return $this->nextPosDate;
    }

    

      /**
     * Set datePos
     *
     * @param \DateTime $datePos
     *
     * @return Pos
     */
    public function setDatePos($datePos)
    {
        $this->datePos = $datePos;

        return $this;
    }

    /**
     * Get datePos
     *
     * @return \DateTime
     */
    public function getDatePos()
    {
        return $this->datePos;
    }




    /**
     * Set resolutionDate
     *
     * @param \DateTime $resolutionDate
     *
     * @return Pos
     */
    public function setResolutionDate($resolutionDate)
    {
        $this->resolutionDate = $resolutionDate;

        return $this;
    }

    /**
     * Get resolutionDate
     *
     * @return \DateTime
     */
    public function getResolutionDate()
    {
        return $this->resolutionDate;
    }

    /**
     * Set realizedAction
     *
     * @param string $realizedAction
     *
     * @return Pos
     */
    public function setRealizedAction($realizedAction)
    {
        $this->realizedAction = $realizedAction;

        return $this;
    }

    /**
     * Get realizedAction
     *
     * @return string
     */
    public function getRealizedAction()
    {
        return $this->realizedAction;
    }

    /**
     * Set nextAction
     *
     * @param string $nextAction
     *
     * @return Pos
     */
    public function setNextAction($nextAction)
    {
        $this->nextAction = $nextAction;

        return $this;
    }

    /**
     * Get nextAction
     *
     * @return string
     */
    public function getNextAction()
    {
        return $this->nextAction;
    }

    /**
     * Set idIncident
     *
     * @param integer $idIncident
     *
     * @return Pos
     */
    public function setIncident($incident)
    {
        $this->incident = $incident;

        return $this;
    }

    /**
     * Get idIncident
     *
     * @return Incident
     */
    public function getIncident()
    {
        return $this->incident;
    }

    /**
     * Set userDeclare
     *
     * @param integer $userDeclare
     *
     * @return Pos
     */
    public function setUserDeclare($userDeclare)
    {
        $this->userDeclare = $userDeclare;

        return $this;
    }

    /**
     * Get userDeclare
     *
     * @return int
     */
    public function getUserDeclare()
    {
        return $this->userDeclare;
    }
}
