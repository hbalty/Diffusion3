<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Application;
use AppBundle\Entity\Client;
use AppBundle\Entity\MailingList;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\Criteria;

/**
 * Metier
 *
 * @UniqueEntity(
 *     fields={"client","nomMetier"},
        errorPath = "nomApplication",
 *     message="Le client possède déja un métier avec ce nom"
 * )
 * @ORM\Table(name="metier")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MetierRepository")
 */
class Metier
{





    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     *@Assert\NotBlank(message=" Veuillez choisir un client !")
     *

     * @ORM\ManyToOne(targetEntity="Client", inversedBy="metiers")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */


    private $client;


        /**
         *
         * Get client
         *
         * @return object
         */
        public function getClient()
        {
            return $this->client;
        }


        /**
         * Set client
         *
         * @param object $nomMetier
         *
         * @return Metier
         */
        public function setClient($client)
        {
            $this->client = $client;

            return $this;
        }


    /**
     * @var string
     *@Assert\NotBlank(message="Le nom de du métier ne dois pas être vide !")
     * @ORM\Column(name="nomMetier", type="string", length=255)
     */
    private $nomMetier;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activationStatus", type="boolean")
     */
    private $activationStatus;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }




    /**
     * Set nomMetier
     *
     * @param string $nomMetier
     *
     * @return Metier
     */
    public function setNomMetier($nomMetier)
    {
        $this->nomMetier = $nomMetier;

        return $this;
    }


    public function getActivatedApplications(){
          $criteria = Criteria::create()->where(Criteria::expr()->eq("activationStatus", true));
          return $this->applications->matching($criteria) ;
    }

    /**
     * Get nomMetier
     *
     * @return string
     */
    public function getNomMetier()
    {
        return $this->nomMetier;
    }

    /**
     * Set activationStatus
     *
     * @param boolean $activationStatus
     *
     * @return Metier
     */
    public function setActivationStatus($activationStatus)
    {
        $this->activationStatus = $activationStatus;

        return $this;
    }

    /**
     * Get activationStatus
     *
     * @return boolean
     */
    public function getActivationStatus()
    {
        return $this->activationStatus;
    }

    /**
     *@Assert\NotBlank(message="Veuillez indiquer les listes de diffusions !")
     * @var string
     * 
     * @ORM\Column(name="mailingLists", type="string")
     */
    private $mailingLists ;

    
    
     /**
     * Set nomMetier
     *
     * @param string $mailingLists
     *
     * @return Metier
     */
    public function setMailingLists($mailingLists)
    {
        $this->mailingLists = str_replace(' ','',$mailingLists);
        return $this;
    }
   

    /**
     * Get mailingLists
     *
     * @return string
     */
    public function getMailingLists()
    {
        return $this->mailingLists;
    }


    /**
    *@Assert\NotBlank(message="Veuillez choisir au moins un application ! ")
    * Plusieurs métiers peuvent avoir plusieurs applications.
    * @ORM\ManyToMany(targetEntity="Application", inversedBy="metiers",cascade={"persist","remove"})
    * @ORM\JoinTable(name="buisiness_applications")
    */

    private $applications ;

    public function __construct(){
      $this->applications = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addApplication(Application $application)
    {
        $application->addMetier($this); // synchronously updating inverse side
        $this->applications[] = $application;
    }



    public function getApplications(){
      return $this->applications;
    }

    public function setApplications(Application $applications){
       $this->applications = $applications;
       return $this;
    }


}
