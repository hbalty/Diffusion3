<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Template
 *
 * @ORM\Table(name="template")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TemplateRepository")
 */
class Template
{

  public function __construct(){
    $this->variables = new ArrayCollection();
    $this->categorie = new ArrayCollection();
    $this->activationStatus = true;
    $this->dateCreation = new \DateTime();
  }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


  /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCreation", type="datetime",nullable=false)
     */
    private $dateCreation;


      /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }


    /**
     * Many Template have One Client.
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * @Assert\NotBlank(message="Veuillez renseigner la client")
     */
    private $client;





    /**
     * One Template has One emetteur/créateur.
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */

    private $emetteur;

    /**
     * @var string
     *@Assert\NotBlank(message="Veuillez renseigner les adresses d'envois.")
     * @ORM\Column(name="destinataires", type="text")
     */
    private $destinataires;

    /**
     * @var string
     *@Assert\NotBlank(message="Le sujet ne doit pas être vide.")
     * @ORM\Column(name="sujet", type="text")
     */
    private $sujet;


    /**
     * Many Templates have Many Categories.
     * @ORM\ManyToMany(targetEntity="Categorie")
     * @ORM\JoinTable(name="categories_templates",
     *      joinColumns={@ORM\JoinColumn(name="template_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="categorie_id", referencedColumnName="id")}
     *      )
     * 
     */
    private $categorie;


    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @var text
     *@Assert\NotBlank(message="Le template ne doit pas être vide.")
     * @ORM\Column(name="template", type="text")
     */
    private $template;
    
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="activationStatus", type="boolean")
     */
    private $activationStatus;
    
    
    /**
     * Set activationStatus
     *
     * @param boolean $activationStatus
     *
     * @return Application
     */

    public function setActivationStatus($activationStatus)
    {
        $this->activationStatus = $activationStatus;

        return $this;
    }

    /**
     * Get activationStatus
     *
     * @return boolean
     */
    public function getActivationStatus()
    {
        return $this->activationStatus;
    }

    /**
     * Many Templates have Many variables.
     * @ORM\ManyToMany(targetEntity="Variable",cascade={"persist","remove"})
     * @ORM\JoinTable(name="template_variables",
     *      joinColumns={@ORM\JoinColumn(name="template_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="variable_id", referencedColumnName="id", unique=true,onDelete="CASCADE")}
     *      )
     */
    private $variables;


    /**
     * @var string
     *
     * @ORM\Column(name="destEnCopie", type="text", nullable=true)
     */
    private $destEnCopie;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param \stdClass $client
     *
     * @return Template
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \stdClass
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set emetteur
     *
     * @param string $emetteur
     *
     * @return Template
     */
    public function setEmetteur($emetteur)
    {
        $this->emetteur = $emetteur;

        return $this;
    }

    /**
     * Get emetteur
     *
     * @return string
     */
    public function getEmetteur()
    {
        return $this->emetteur;
    }

    /**
     * Set destinataires
     *
     * @param string $destinataires
     *
     * @return Template
     */
    public function setDestinataires($destinataires)
    {
        $this->destinataires = $destinataires;

        return $this;
    }

    /**
     * Get destinataires
     *
     * @return string
     */
    public function getDestinataires()
    {
        return $this->destinataires;
    }

    /**
     * Set sujet
     *
     * @param string $sujet
     *
     * @return Template
     */
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;

        return $this;
    }


    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return Template
     */
    public function setCategorie($categorie)
    {
        $this->categorie  = $categorie;

        return $this;
    }

    /**
     * Get sujet
     *
     * @return string
     */
    public function getSujet()
    {
        return $this->sujet;
    }




    /**
     * Get categorie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return Template
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set template
     *
     * @param string $template
     *
     * @return Template
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set variables
     *
     * @param array $variables
     *
     * @return Template
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;

        return $this;
    }



    /**
     * Get variables
     *
     * @return array
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * Set destEnCopie
     *
     * @param string $destEnCopie
     *
     * @return Template
     */
    public function setDestEnCopie($destEnCopie)
    {
        $this->destEnCopie = $destEnCopie;

        return $this;
    }

    /**
     * Get destEnCopie
     *
     * @return string
     */
    public function getDestEnCopie()
    {
        return $this->destEnCopie;
    }
}
