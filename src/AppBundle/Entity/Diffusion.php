<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diffusion
 *
 * @ORM\Table(name="diffusion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DiffusionRepository")
 */
class Diffusion
{
    
    
     public function __construct(){
            $this->destinataire = "diffusion-noreply@cdiscount.com";
            $this->dateEnvoi = new \DateTime();
     }

        
        
        
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Many Mails have One incidents.
     * @ORM\ManyToOne(targetEntity="Incident", inversedBy="diffusions")
     * @ORM\JoinColumn(name="id_incident", referencedColumnName="id")
     */
    private $incident;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="client", type="object")
     */
    private $client ;
    
    

    
     /**
     * @var string
     *
     * @ORM\Column(name="destImpact", type="string", length=1000)
     */ 
    private $destImpact; 
    

     /**
     * @var string
     *
     * @ORM\Column(name="destEnCopie", type="string", length=1000)
     */
    private $destEnCopie;

    /**
     * @var string
     *
     * @ORM\Column(name="sujet", type="string", length=255)
     */
    private $sujet;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="string", length=10000)
     */
    private $contenu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnvoi", type="datetime")
     */
    private $dateEnvoi;

     /**
    * Many emails have One sender.
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
     
    private $emetteur;


     /**
     * Set destImpact
     *
     * @param string $destImpact
     *
     * @return Diffusion
     */
    public function setDestImpact($destImpact)
    {
        $this->destImpact = $destImpact;

        return $this;
    }

    /**
     * Get destImpact
     *
     * @return string
     */
    public function getDestImpact()
    {
        return $this->destImpact;
    }

     
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
     /**
     * Set destinataire
     *
     * @param string $destinataire
     *
     * @return Diffusion
     */
    public function setDestinataire($destinataire)
    {
        $this->destinataire = $destinataire;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getDestinataire()
    {
        return $this->destinataire;
    }
  
    
    
    /**
     * Set incident
     *
     * @param \stdClass $incident
     *
     * @return Diffusion
     */
    public function setIncident($incident)
    {
        $this->incident = $incident;

        return $this;
    }

    /**
     * Get incident
     *
     * @return \stdClass
     */
    public function getIncident()
    {
        return $this->incident;
    }

    /**
     * Set client
     *
     * @param \stdClass $client
     *
     * @return Diffusion
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \stdClass
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set destEnCopie
     *
     * @param string $destEnCopie
     *
     * @return Diffusion
     */
    public function setDestEnCopie($destEnCopie)
    {
        $this->destEnCopie = $destEnCopie;

        return $this;
    }

    /**
     * Get destEnCopie
     *
     * @return array
     */
    public function getDestEnCopie()
    {
        return $this->destEnCopie;
    }

    /**
     * Set sujet
     *
     * @param string $sujet
     *
     * @return Diffusion
     */
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;

        return $this;
    }

    /**
     * Get sujet
     *
     * @return string
     */
    public function getSujet()
    {
        return $this->sujet;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Diffusion
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set dateEnvoi
     *
     * @param \DateTime $dateEnvoi
     *
     * @return Diffusion
     */
    public function setDateEnvoi($dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;

        return $this;
    }

    /**
     * Get dateEnvoi
     *
     * @return \DateTime
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * Set emetteur
     *
     * @param \stdClass $emetteur
     *
     * @return Diffusion
     */
    public function setEmetteur($emetteur)
    {
        $this->emetteur = $emetteur;

        return $this;
    }

    /**
     * Get emetteur
     *
     * @return \stdClass
     */
    public function getEmetteur()
    {
        return $this->emetteur;
    }
}

